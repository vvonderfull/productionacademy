<?php
$params = array_merge(
    require __DIR__ . '/../../common/config/params.php',
    require __DIR__ . '/../../common/config/params-local.php',
    require __DIR__ . '/params.php',
    require __DIR__ . '/params-local.php'
);

return [
    'id' => 'app-api',
    'basePath' => dirname(__DIR__),
    'bootstrap' => ['log'],

    'controllerNamespace' => 'api\controllers',

    'modules' => [
        'v1' => [
            'basePath' => '@app/modules/v1',
            'class' => 'api\modules\v1\MVPModule',
        ],
        'v2' => [
            'basePath' => '@app/modules/v2',
            'class' => 'api\modules\v2\SecondModule',
        ]
    ],

    'components' => [
        'request' => [
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
                'text/xml' => 'yii\web\XmlParser',
            ],
        ],

        'user' => [
            'identityClass' => 'common\models\User',
//            'enableAutoLogin' => true,
//            'identityCookie' => ['name' => '_identity-frontend', 'httpOnly' => true],
            'enableSession' => false,
            'enableAutoLogin' => false,
        ],

        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
         ],

        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'rules' => [
                '' => 'v1/user/index',

                //admin students actions
                '/get-all-students' => 'v1/user/get-all-students',
                '/get-all-students-in-set' => 'v1/user/get-all-students-in-set',
                '/get-student' => 'v1/user/get-user',
                '/get-hermits'=> 'v1/user/get-hermits',
                '/send-mails' => 'v1/user/send-mails',
                '/leads' => 'v1/user/get-count-lead',
                '/get-excel-roles' => 'v1/user/get-excel-roles',
                '/get-excel-teams' => 'v1/user/get-excel-teams',

                //admin teams actions
                '/change-team' => 'v1/team/change-user-team',
                '/change-status-team' => 'v1/team/change-status-team',
                '/disband-team' => 'v1/team/disband-team',
                '/change-team-name'=> 'v1/team/change-team-name',
                '/form-teams' => 'v1/team/form-teams',

                //user registration and authorization
                '/mail' => 'v1/user/sign-up',
                '/sign-up' => 'v1/user/sign-up-second',
                '/login' => 'v1/user/login',
                '/send-token' => 'v1/user/send-token',
                '/change-path-to-pruff' => 'v1/user/change-path-to-pruff',
                '/get-link-to-drive' => 'v1/user/get-link-to-drive',

                //user test
                '/test/start-test' => 'v1/test/start-test',
                '/test/set-answer' => 'v1/test/set-answer',
                '/test/calculate-result' => 'v1/test/calculate-result',
                '/test/get-answer' => 'v1/test/get-answer',
                '/test/get-all-questions' => 'v1/test/get-all-questions',
                '/get-all-tests' => 'v1/test/get-all-tests',
                '/test/set-result-to-test' => 'v1/test/set-result-to-test',
                '/test/finish-tests' => 'v1/test/finish-tests',
                '/test/get-test-results' => 'v1/test/get-test-results',

                [
                    'class' => 'yii\rest\UrlRule',
                    'controller' => ['v1/user'],
                    'extraPatterns' => [
                    ],
                ],

            ],
        ],

    ],
    'params' => $params,
];
