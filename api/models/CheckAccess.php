<?php


namespace api\models;

use common\models\User;
use yii\base\Model;

class CheckAccess
{
   // TODO Перенести всю логику проверки доступа на Rbac.
    public function checkRules($token)
    {
        //Небольшой костыль на проверку токена (если он задается в query парамс, или же в bearer-token)
        if ($token == null) {
            $user = $this->checkToken(true,'');
        }
        else {
            $user = $this->checkToken(false,$token);
        }
        if ($user != null && $this->checkStatus($user)) {
            return true;
        }
        return false;
    }

    public function checkToken($is_bearer, $token_text)
    {
        $bearer= new Token();
        //Вытаскиваем токен из заголовка
        if ($is_bearer == true)
        {
            $token = $bearer->getAuthToken();
        }
        //Или возвращаем таким, какой он задается в параметрах.
        else {
            $token = $token_text;
        }
        if ($token == null) {
            return null;
        }
        else {
            $user = $bearer->findIdentityByAccessToken($token);
            if ($user != null)
                return $user;
            return null;
        }
    }

    //Подобие Rbaс. Проверка на статус пользователя
    protected  function checkStatus($user)
    {
        if ($user->status == User::STATUS_MENTOR ||
            $user->status == User::STATUS_SUPERVISOR ||
            $user->status == User::STATUS_SUPERMENTOR)
                return true;
        return false;
    }
}