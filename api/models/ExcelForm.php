<?php


namespace api\models;

use common\models\User;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use yii\base\Security;
use yii\db\Query;

//Работа с экселькой
class ExcelForm
{
    protected function getStudents($role_id)
    {
        //массив студентов
        $students = (new Query())
            ->select(['user.id','user.fio','user.total_point','user.email','user.phone','user.study_place','user.age','user.link_to_drive'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->andWhere(['public.user.is_del'=> false])
            ->all();

        $leads = (new Query())
            ->select(['user.id','user.fio','user.total_point','user.email','user.phone','user.study_place','user.age','user.link_to_drive'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_LEAD])
            ->andWhere(['public.user.is_del'=> false])
            ->andWhere(['!=','public.user.password_hash', ''])
            ->all();

        $user_role = new UserRole();
        $temp = array();
        foreach ($students as $i => $i) {
            $role = $user_role->getLastUserRole($students[$i]['id']);
            if ($role['role_id'] == $role_id) {
                array_push($temp, $students[$i]);
            }
        }

        foreach ($leads as $i => $i) {
            $role = $user_role->getLastUserRole($leads[$i]['id']);
            if ($role['role_id'] == $role_id) {
                array_push($temp, $leads[$i]);
            }
        }

        foreach ($temp as $i => $i)
        {
            $temp[$i]['1-1'] = $user_role->getCountPoints($temp[$i]['id'],7);
            $temp[$i]['1-2'] = $user_role->getCountPoints($temp[$i]['id'],8);
            $temp[$i]['1-3'] = $user_role->getCountPoints($temp[$i]['id'],9);
            $temp[$i]['1-4'] = $user_role->getCountPoints($temp[$i]['id'],10);
            $temp[$i]['1-5'] = $user_role->getCountPoints($temp[$i]['id'],11);

            $temp[$i]['1-total'] = $user_role->getCountTotalPointsForFirstStage($temp[$i]['id']);

            $secondTest = (new Test())->getTestsForSecondStage($temp[$i]['id']);
            foreach ($secondTest as $j => $j)
            {
                $temp[$i]['2-'. ($j+1)] = $user_role->getCountPoints($temp[$i]['id'],$secondTest[$j]['id']);
            }
            if (!empty($secondTest)) {
                $temp[$i]['2-total'] = $user_role->getCountTotalPointsForSecondStage($temp[$i]['id']);
            }
        }
        return $temp;
    }


    //Достаем из массива объектов необходимые данные по столбцу
    protected function getColumn($student,$j)
    {
        switch ($j)
        {
            case 0:
                return $student['fio'];
                break;
            case 1:
                return $student['total_point'];
                break;
            case 2:
                return $student['email'];
                break;
            case 3:
                return $student['phone'];
                break;
            case 4:
                return $student['study_place'];
                break;
            case 5:
                return $student['age'];
                break;
            case 6:
                return $student['link_to_drive'];
                break;
            case 7:
                return $student['1-1'];
                break;
            case 8:
                return $student['1-2'];
                break;
            case 9:
                return $student['1-3'];
                break;
            case 10:
                return $student['1-4'];
                break;
            case 11:
                return $student['1-5'];
                break;
            case 12:
                return $student['1-total'];
                break;
            case 13:
                if (array_key_exists('2-1',$student)) {
                    return $student['2-1'];
                }
                break;
            case 14:
                if (array_key_exists('2-2',$student)) {
                    return $student['2-2'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 15:
                if (array_key_exists('2-3',$student)) {
                    return $student['2-3'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 16:
                if (array_key_exists('2-4',$student)) {
                    return $student['2-4'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 17:
                if (array_key_exists('2-5',$student)) {
                    return $student['2-5'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 18:
                if (array_key_exists('2-6',$student)) {
                    return $student['2-6'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 19:
                if (array_key_exists('2-7',$student)) {
                    return $student['2-7'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 20:
                if (array_key_exists('2-8',$student)) {
                    return $student['2-8'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
            case 21:
                if (array_key_exists('2-9',$student)) {
                    return $student['2-9'];
                    break;
                }
                else {
                    return $student['2-total'];
                    break;
                }
        }
    }

    protected function getColumnForLeads($lead,$j)
    {
        switch ($j) {
            case 0:
                return $lead['fio'];
                break;
            case 1:
                return $lead['email'];
                break;
            case 2:
                return $lead['phone'];
                break;
        }
    }

    protected function setStudentsInRows($sheet,$students,$columns)
    {
        $rows = count($students);
        for ($i = 2 ; $i <= $rows+1 ; $i++)
        {
            for ($j = 1 ; $j <= $columns ; $j++)
            {
                $sheet->setCellValueByColumnAndRow(
                    $j,
                    $i,
                    $this->getColumn($students[$i-2],$j-1)
                );

                $sheet->getCellByColumnAndRow($j, $i)
                    ->getStyle()
                    ->getAlignment()
                    ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                if ($j == 7)
                {
                    $sheet->getCellByColumnAndRow($j, $i)
                        ->getHyperLink()
                        ->setUrl($this->getColumn($students[$i-2],$j-1));
                }
            }
        }
    }

    protected function setHeaders($sheet,$role_id)
    {
        $sheet->setCellValue('A1','ФИО практиканта');
        $sheet->setCellValue('B1','Общий балл за все тесты');
        $sheet->setCellValue('C1','Почта');
        $sheet->setCellValue('D1','Телефон');
        $sheet->setCellValue('E1','Учебное заведение');
        $sheet->setCellValue('F1','Возраст');
        $sheet->setCellValue('G1','Ссылка на скрины');
        $sheet->setCellValue('H1','Уступок');
        $sheet->setCellValue('I1','Противоборство');
        $sheet->setCellValue('J1','Сотрудничество');
        $sheet->setCellValue('K1','Компромисс');
        $sheet->setCellValue('L1','Избегание');
        $sheet->setCellValue('M1','Общий балл за 1-ый этап');

        $numColumns = 13;
        $tests = (new Query())
            ->select(['test.id', 'test.path', 'test.max_points', 'time_to_test_in_minutes'])
            ->from('{{test}}')
            ->join('LEFT JOIN', '{{public.test_role}}', 'test.id = test_role.test_id')
            ->where(['test.stage' => 3])
            ->andWhere(['test_role.role_id' => $role_id])
            ->all();

        $letter = 'N';
        if ($tests != null)
        {
            foreach ($tests as $i => $i) {
                $sheet->setCellValue($letter . '1', 'Балл за ' . ($i + 1) . ' тест второго этапа');
                $numColumns++;
                $letter++;
            }
            $sheet->setCellValue($letter . '1', 'Общий балл за 2-ой этап');
            $numColumns++;
        }

        foreach (range('A',$letter) as $l) {
            $sheet->getColumnDimension($l)->setAutoSize(true);
        }

        $styleHeaders = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];

        $sheet->getStyle('A1:' .$letter. '1')->applyFromArray($styleHeaders);
        return $numColumns;
    }

    protected function setHeadersForTeams($sheet, $row)
    {
        $sheet->setCellValue('B'.$row,'№');
        $sheet->setCellValue('C'.$row,'ФИО практиканта');
        $sheet->setCellValue('D'.$row,'Должность');
        $sheet->setCellValue('E'.$row,'Название проекта');
        $sheet->setCellValue('F'.$row,'Название команды');
        $sheet->setCellValue('G'.$row,'Почта');
        $sheet->setCellValue('H'.$row,'Телефон');
        $sheet->setCellValue('I'.$row,'Учебное заведение');
        $sheet->setCellValue('J'.$row,'Общие баллы за тесты');

        foreach (range('B','J') as $l) {
            $sheet->getColumnDimension($l)->setAutoSize(true);
        }

        $styleHeaders = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT,
            ],
            'borders' => [
                'outline' => [
                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
                ],


//                'left' => [
//                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
//                ],
//                'right' => [
//                    'borderStyle' => \PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN,
//                ],
            ],
        ];

        $sheet->getStyle('B'.$row.':J'.$row)
            ->getFill()
            ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
            ->getStartColor()->setARGB('FFA0A0A0');;

        $sheet->getStyle('B'.$row.':J'.$row)->applyFromArray($styleHeaders);
    }

    public function formPage($spreadsheet,$role_id,$title)
    {
        if ($role_id == 1)
        {
            $Sheet = $spreadsheet->getActiveSheet();
            $Sheet->setTitle($title);
        }
        else {
            $Sheet = $spreadsheet->createSheet();
            $Sheet->setTitle($title);
        }

        $numberOfColumns = $this->setHeaders($Sheet,$role_id);
        $students = $this->getStudents($role_id);

        if ($students != null)
        {                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       
            $this->setStudentsInRows($Sheet,$students,$numberOfColumns);
        }
    }

    protected function formLeadPage($spreadsheet)
    {
        $sheet = $spreadsheet->createSheet();
        $sheet->setTitle('Не прошедшие регистрацию');

        $sheet->setCellValue('A1','ФИО практиканта');
        $sheet->setCellValue('B1','Почта');
        $sheet->setCellValue('C1','Телефон');

        $styleHeaders = [
            'font' => [
                'bold' => true,
            ],
            'alignment' => [
                'horizontal' => \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER,
            ]
        ];
        $sheet->getColumnDimension('A')->setAutoSize(true);
        $sheet->getColumnDimension('B')->setAutoSize(true);
        $sheet->getColumnDimension('C')->setAutoSize(true);
        $sheet->getStyle('A1:C1')->applyFromArray($styleHeaders);

        $leads = (new Query())
            ->select(['user.fio','user.email','user.phone'])
            ->from('{{%user}}')
            ->where(['public.user.status'=> User::STATUS_LEAD])
            ->andWhere(['public.user.password_hash' => null])
            ->andWhere(['public.user.is_del'=>false])
            ->all();
        $rows = count($leads);

        for ($i = 2 ; $i <= $rows+1 ; $i++)
        {
            for ($j = 1 ; $j <= 3 ; $j++)
            {
                $sheet->setCellValueByColumnAndRow(
                    $j,
                    $i,
                    $this->getcolumnForLeads($leads[$i-2],$j-1)
                );
            }
        }

    }

    public function formExcelOnRoles()
    {
        //Объявили шаблон
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);

        //Страница менеджеров
        $this->formPage($spreadsheet,1,'Менеджеры');

        //Страница аналитиков
        $this->formPage($spreadsheet,2,'Аналитики');

        //Страница Front-End разработчиков
        $this->formPage($spreadsheet,3,'Front-end разработчики');

        //Страница Back-end разработчиков
        $this->formPage($spreadsheet,4,'Back-end разработчики');

        //Страница тестировщиков
        $this->formPage($spreadsheet,5,'Тестировщики');

        //Страница дизайнеров
        $this->formPage($spreadsheet,6,'Дизайнеры');

        //Страница лидов
        $this->formLeadPage($spreadsheet);

        $spreadsheet->setActiveSheetIndex(0);

        //Сохранение на вебсервере.
//        $writer = new Xlsx($spreadsheet);
//        $writer->save('excel/students.xlsx');
//        return (['message'=> 'Файл сгенерирован и готов к использованию']);

          // to client excel file
//        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
//        header('Content-Disposition: attachment;filename="students.xlsx"');
//        header('Cache-Control: max-age=0');

//        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
//        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
//        header ( "Cache-Control: no-cache, must-revalidate" );
//        header ( "Pragma: no-cache" );
//        header ( "Content-type: application/vnd.ms-excel" );
//        header ( "Content-Disposition: attachment; filename=roles.xls" );
//
//
//        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
//        $writer->save('php://output');
//

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=roles.xls" );
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');



        exit;

//        //to client browser
//        header('Content-Type: application/vnd.ms-excel');
//        header('Content-Disposition: attachment;filename="students.xls"');
//        header('Cache-Control: max-age=0');
//
//        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
//        $writer->save('php://output');

//        return (['message'=> 'Файл сгенерирован и готов к использованию']);
    }

    public function formExcelOnTeams()
    {
        //Объявили шаблон
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getDefaultStyle()->getFont()->setName('Arial');
        $spreadsheet->getDefaultStyle()->getFont()->setSize(10);

        $this->formTeamPage($spreadsheet);

//        $Sheet = $spreadsheet->getActiveSheet();


//        $writer = new Xlsx($spreadsheet);
//        $writer->save('excel/teams.xlsx');

        header ( "Expires: Mon, 1 Apr 1974 05:00:00 GMT" );
        header ( "Last-Modified: " . gmdate("D,d M YH:i:s") . " GMT" );
        header ( "Cache-Control: no-cache, must-revalidate" );
        header ( "Pragma: no-cache" );
        header ( "Content-type: application/vnd.ms-excel" );
        header ( "Content-Disposition: attachment; filename=teams.xls" );
        $writer = \PhpOffice\PhpSpreadsheet\IOFactory::createWriter($spreadsheet, 'Xls');
        $writer->save('php://output');
        exit;
//        return (['message'=> 'Файл сгенерирован и готов к использованию']);
    }

    public function formTeamPage($spreadsheet)
    {
        $Sheet = $spreadsheet->getActiveSheet();
        $Sheet->setTitle('Команды');

        $Sheet->setCellValue("B2", 'Общие списки практикантов в проекте Smartworld Academy.');
        $Sheet->getStyle('B2')->getFill()->setFillType(
            \PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID);
        $Sheet->getStyle('B2')->getFill()->getStartColor()->setRGB('EEEEEE');
        $Sheet->mergeCells('B2:J2');
        // Выравнивание текста
        $Sheet->getStyle('B2')->getAlignment()->setHorizontal(
            \PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_CENTER);

        $Sheet->getStyle('B2')->getAlignment()->setVertical(
            \PhpOffice\PhpSpreadsheet\Style\Alignment::VERTICAL_CENTER);

        $Sheet->getStyle('B2')->getFont()->setBold(true);
        $Sheet->getStyle('B2')->getFont()->setSize(18);

        $Sheet->getRowDimension('2')->setRowHeight(25);

        $teams = $this->getStudentsInTeam();
        if ($teams != null)
        {
            $this->setTeamsInRows($Sheet,$teams);
        }
    }

    protected function setTeamsInRows($sheet,$teams)
    {
        //Начальные значения
        $col = 2; $row = 4;

        foreach ($teams as $index => $index) {
            $this->setHeadersForTeams($sheet, strval($row));
            $count = count($teams[$index]['students']);
            for ($i = $row + 1; $i <= $row + $count ; $i++) {
                for ($j = $col; $j <= 8 + $col; $j++) {
                    $sheet->setCellValueByColumnAndRow(
                        $j,
                        $i,
                        $this->getColumnForTeams($teams[$index],$i-$row-1, $j-$col)
                    );

                    $sheet->getCellByColumnAndRow($j, $i)
                        ->getStyle()
                        ->getFill()
                        ->setFillType(\PhpOffice\PhpSpreadsheet\Style\Fill::FILL_SOLID)
                        ->getStartColor()->setRGB('EEEEEE');

                    $sheet->getCellByColumnAndRow($j, $i)
                        ->getStyle()
                        ->getAlignment()
                        ->setHorizontal(\PhpOffice\PhpSpreadsheet\Style\Alignment::HORIZONTAL_LEFT);

                    $sheet->getCellByColumnAndRow($j, $i)
                        ->getStyle()
                        ->getBorders()
                        ->getOutline()
                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

//                    $sheet->getCellByColumnAndRow($j, $i)
//                        ->getStyle()
//                        ->getBorders()
//                        ->getLeft()
//                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);
//
//                    $sheet->getCellByColumnAndRow($j, $i)
//                        ->getStyle()
//                        ->getBorders()
//                        ->getRight()
//                        ->setBorderStyle(\PhpOffice\PhpSpreadsheet\Style\Border::BORDER_THIN);

                    // Делаем поле - текстовым
//                    if ($j == 8)
//                    {
//                        $sheet->getCellByColumnAndRow($j, $i)
//                            ->get()
//                            ->
//                    }
                }
            }
            $row += $count + 3;
        }
    }

    protected function getColumnForTeams($team,$i,$j)
    {
        switch ($j) {
            case 0:
                return $i+1;
                break;
            case 1:
                return $team['students'][$i]['fio'];
                break;
            case 2:
                return $team['students'][$i]['role']['name'];
                break;
            case 3:
                return $team['project'];
                break;
            case 4:
                return $team['team_name'];
                break;
            case 5:
                return $team['students'][$i]['email'];
                break;
            case 6:
                return $team['students'][$i]['phone'];
                break;
            case 7:
                return $team['students'][$i]['study_place'];
                break;
            case 8:
                return $team['students'][$i]['total_point'];
                break;
        }
    }

    protected function getStudentsInTeam()
    {
        //Вывод всех команд
        $teams = (new Query())
            ->select(['team.id AS team_id', 'team.name AS team_name', 'team.project'])
            ->from('{{team}}')
            ->where(['public.team.inSet' => true])
            ->andWhere(['public.team.isDel' => false])
            ->orderBy('team_id')
            ->all();

        $students = (new Query())
            ->select(['user.id', 'user.fio', 'user.total_point', 'user.study_place', 'user.email', 'user.phone'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->andWhere(['public.user.is_del' => false])
            ->orderBy('user.total_point DESC')
            ->all();

        //Заполнение команд их студентами
        foreach ($teams as $index => $index) {
            $temp = array();
            foreach ($students as $i => $i) {
                $students[$i]['role'] = (new Student())->getLastRole($students[$i]['id']);
                $team = (new Student())->getLastTeam($students[$i]['id']);
                if ($team['team_id'] == $teams[$index]['team_id']) {
                    array_push($temp, $students[$i]);
                }
            }
            //Сортируем команду по баллам...
            if ($teams[$index]['team_id'] == Team::SINGLES_id) {
                continue;
            } // Сортируем остальные команды по ролям
            else {
                usort($temp, function ($a, $b) {
                    return ($a['role']['role_id'] - $b['role']['role_id']);
                });
            }
            $teams[$index]['students'] = $temp;
        }
        return $teams;
    }
}