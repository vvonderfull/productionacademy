<?php

namespace api\models;

use api\models\Token;
use common\models\User;
use DateTime;
use Yii;
use yii\base\Model;

class LoginForm extends Model
{
    public $email;
    public $password;
    private $_user;

    public function rules()
    {
        return [
            //Было раньше
//            [['password','password_hash'], 'string'],
            [['email','password'], 'string'],
            [['email', 'password'], 'required'],
            // password is validated by validatePassword()
            ['password', 'validatePassword'],
        ];
    }

//    /**
//     * Validates the password.
//     * This method serves as the inline validation for password.
//     *
//     * @param string $attribute the attribute currently being validated
//     * @param array $params the additional name-value pairs given in the rule
//     */
//    public function validatePassword($attribute, $params)
    public function validatePassword()
    {
        if (!$this->hasErrors())
        {
            $user = $this->getUser();
            if (!$user || !$user->validatePassword($this->password))
            {
//                return (['message'=>'Пароль или логин не совпадает']);
                $this->addError('message', 'Пароль или логин не совпадает.');
            }
        }
    }

    public function login()
    {
        if (!$this->validate())
        {
            return $this->getErrors();
        }
        else {
            $user_token = Token::findOne(['user_id' => $this->getUser()->id]);

            $now = date("Y-m-d H:i:s");
            $expired_time = $user_token->expired_at;

            $interval = (new SignUpFullForm())->dateDifference($now, $expired_time);

            //Проверка на истечение срока токена
            if ($interval < 1) {
                return ['token' => $user_token->token, 'status' => $this->getUser()->status];
            } else {
                $user_token->generateToken(date("Y-m-d H:i:s", strtotime("+1 day")));
                $user_token->save();
                return ['token' => $user_token->token, 'status' => $this->getUser()->status];
            }
        }
    }
    
    /**
     * Finds user by [email]
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null)
        {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}
