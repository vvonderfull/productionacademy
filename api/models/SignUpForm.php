<?php


namespace api\models;


use api\models\Token;
use api\models\Role;
use common\models\User;
use Yii;
use yii\base\Model;

class SignUpForm extends Model
{
    public $email;
    public $phone;
    public $fio;

    private $_user;

    public function rules()
    {
        return [
            // username and password are both required
            [['email', 'phone', 'fio'], 'required'],
            [['email', 'phone', 'fio'], 'string'],
            ['email', 'email'],
            ['email', 'string' , 'max' => 255],
            ['email', 'unique', 'targetClass' => '\common\models\User', 'message' => 'Такой email уже используется.'],
        ];
    }

    public function signUp()
    {
        if (!$this->validate())
        {
            return $this->getErrors();
        }
        $_user = new User;
        $_user->status = User::STATUS_LEAD;
        $_user->email = $this->email;
        $_user->phone = $this->phone;
        $_user->fio = $this->fio;
        if ($_user->save() && $this->generateTokenToUser()) {
            return ['message' => 'Вы успешно зарегистрированны'];
        }
    }

    protected function generateTokenToUser()
    {
        $token = new Token();
        $token->user_id = $this->getUser()->id;
        $date = date("Y-m-d H:i:s", strtotime("+1 day"));
        $token->generateToken($date);
        return $token->save() ? $token : false;
    }

    protected function getUser()
    {
        if ($this->_user === null)
        {
            $this->_user = User::findByEmail($this->email);
        }
        return $this->_user;
    }
}