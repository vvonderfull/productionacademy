<?php

namespace api\models;

use api\models\Token;
use api\models\Role;
use api\models\UserRole;
use common\models\User;
use Yii;
use yii\base\Model;
use api\models\CheckAccess;
use yii\behaviors\TimestampBehavior;

class SignUpFullForm extends Model
{
    //TODO перейти на bearer token
    public $token;

    //Есть в базе
    public $email;
    public $phone;
    public $fio;

    //Новые поля
    public $role_name;
    public $password;
    public $age;
    public $experience;
    public $study_place;
    public $period_start;
    public $period_finish;
    public $work_status;
    public $comment;
//    public $confirmed_password;

    private $userRoles;

    public function rules()
    {
        return [
            [['token','study_place','comment'] , 'string'],
            [['period_start','period_finish'], 'date'],
            [['age', 'role_name', 'password'], 'required', 'message' => 'Укажите {attribute}'],
            [['age'], 'integer', 'min' => 18, 'message' => 'Значение поля Возраст должен не мение 18'],
            [['password'], 'string', 'min' => 8, 'max'=> 30],
            [['work_status'], 'boolean'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    public function signUp()
    {
        $_user = User::findByVerificationToken($this->token);
        if ($_user == null)
        {
            return (['message' => 'Вы ввели неверный токен']);
        }

        $_user->age = $this->age;
        $_user->experience = $this->experience;
        $_user->study_place = $this->study_place;
        $_user->period_start = $this->period_start;
        $_user->period_finish = $this->period_finish;
        $_user->work_status = boolval($this->work_status);
        $_user->comment = $this->comment;

        $_user->setPassword($this->password);

        $_role = Role::findOne(['name' => $this->role_name]);
        if (!$_role) {
            return ['message' => 'роли ' . $this->role_name . ' не существует'];
        }
        //Проверяем не регистрировался ли человек уже в этом наборе
        if ($this->checkLastSignUp($_user)) {
            return (['message'=>'Вы уже регистрировались в этом наборе']);
        }

        if ($_user->save() AND ($this->setUserRole($_user,$_role))) {
            return ['message' => 'Регистрация прошла успешно!'];
        }
        return ['user' => $this->getErrors(), 'user_role' => $_role->getErrors()];
    }

    protected function setUserRole($_user,$_role)
    {
        $role = new UserRole();
        $role->user_id = $_user->id;
        $role->role_id = $_role->id;
        $role->test_id = 7;
        $role->test_date = date("Y-m-d H:i:s");
        return $role->save();
    }

    protected function checkLastSignUp($user)
    {
        $user_role = new UserRole();
        $user_role = $user_role->getLastUserRole($user->id);
        $norm_date = date("Y-m-d H:i:s");
        $date = $user_role['test_date'];
        if ($user_role == null)
        {
            return false;
        }
        $interval = $this->dateDifference($norm_date,$date);
        if (intval($interval) < 30) {
            return true;
        }
        else {
            return false;
        }
    }
    public function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
    {
        $datetime1 = date_create($date_1);
        $datetime2 = date_create($date_2);
        $interval = date_diff($datetime1, $datetime2);
        return $interval->format($differenceFormat);
    }
//    /**
//     * @return Token|null
//     */
//    public function auth()
//    {
//        if ($this->validate())
//        {
//            $token = new Token();
//            $token->user_id = $this->getUser()->id;
//            $token->generateToken(time() + 3600 * 24);
//            return $token->save() ? $token : null;
//        } else {
//            return null;
//        }
//    }
    /**
     * Finds user by [[username]]
     *
     * @return User|null
     */
    protected function getUser()
    {
        if ($this->_user === null) {
            $this->_user = User::findByUsername($this->username);
        }

        return $this->_user;
    }
}