<?php


namespace api\models;

use api\models\Role;
use api\models\Team;
use api\models\UserRole;
use api\models\UserTeam;
use common\models\User;
use Yii;
use yii\db\Query;
use yii\helpers\Url;

class Student
{
    //Получение последней команды, в которую перешел студент
    public function getLastTeam($user_id)
    {
        return (new Query())
            ->select(['team_id','created_at'])
            ->from('{{public.user_team}}')
            ->where(['user_team.user_id'=>$user_id])
            ->orderBy('user_team.created_at DESC')
            ->limit(1)
            ->one();
    }

    //Получение последнего прошедшего теста студента
    public function getLastRole($user_id)
    {
        return (new Query())
            ->select(['user_role.id','role.id AS role_id', 'role.name'])
            ->from('{{public.role}}')
            ->join('LEFT JOIN', 'public.user_role', 'public.role.id = public.user_role.role_id')
            ->where(['user_role.user_id'=>$user_id])
            ->orderBy('user_role.test_date DESC')
            ->limit(1)
            ->one();
    }

    public function getAllStudents($in_set)
    {
        //Вывод всех команд
        $teams = (new Query())
            ->select(['team.id AS team_id', 'team.name AS team_name'])
            ->from('{{team}}')
            ->where(['public.team.inSet' => $in_set])
            ->andWhere(['public.team.isDel' => false])
            ->orderBy('team_id')
            ->all();

        //массив студентов
        $students = (new Query())
            ->select(['user.id','user.total_point','user.study_place'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->andWhere(['public.user.is_del'=> false])
//            ->orderBy('user.total_point DESC')
            ->all();
//        return $students;
        //Заполнение команд их студентами
        foreach ($teams as $index => $index) {
            $temp = array();
            foreach ($students as $i => $i) {
                $students[$i]['fio'] = $this->getFio($students[$i]['id']);
                $students[$i]['role'] = $this->getLastRole($students[$i]['id']);
                $team = $this->getLastTeam($students[$i]['id']);
                if ($team['team_id'] == $teams[$index]['team_id']) {
                    array_push($temp, $students[$i]);
                }
            }
            //Сортируем команду по баллам...
            if ($teams[$index]['team_id'] == Team::SINGLES_id) {
                usort($temp, function ($a, $b) {
                    return ($b['total_point'] - $a['total_point']);
                });
            }
            // Сортируем остальные команды по ролям
            else {
                usort($temp, function ($a, $b) {
                    return ($a['role']['role_id'] - $b['role']['role_id']);
                });
            }
            $teams[$index]['students'] = $temp;
        }
        return $teams;
    }

    public function getFio($user_id)
    {
        $user = User::findOne(['id'=>$user_id]);
        $fio = $user->fio;
        $pieces = explode(" ",$fio);

        switch (count($pieces))
        {
            case 0:
                return "";
                break;
            case 2:
                return ucfirst($pieces[0]) . " " . mb_substr($pieces[1], 0, 1, "UTF-8") . ".";
                break;
            case 3:
                return ucfirst($pieces[0]) . " " . mb_substr($pieces[1], 0, 1, "UTF-8") . "." . mb_substr($pieces[2], 0, 1, "UTF-8") . ".";
                break;
            default:
                return ucfirst($pieces[0]);
                break;
        }
    }

    public function formStudents()
    {
        //Вывод всех команд
        $teams = (new Query())
            ->select(['team.id AS team_id', 'team.name AS team_name'])
            ->from('{{team}}')
            ->where(['public.team.inSet' => false])
            ->andWhere(['public.team.isDel' => false])
//            ->andWhere(['!=','team.id',Team::SINGLES_id])
            ->orderBy('team_id')
            ->all();

        //массив студентов
        $students = (new Query())
            ->select(['user.id', 'user.total_point', 'user.study_place'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->andWhere(['public.user.is_del'=>false])
            ->orderBy('user.total_point DESC')
            ->all();

        $singles = array();
        foreach ($students as $index => $index) {
            $students[$index]['fio'] = $this->getFio($students[$index]['id']);
            $students[$index]['role'] = $this->getLastRole($students[$index]['id']);
            $students[$index]['team'] = $this->getLastTeam($students[$index]['id']);
            if ($students[$index]['team']['team_id'] == Team::SINGLES_id)
            {
                array_push($singles,$students[$index]);
            }
        }

        foreach ($teams as $j => $j)
        {
            $temp = array();
            if ($teams[$j]['team_id'] != Team::SINGLES_id)
            {
                // Записали, кто есть в команде
                foreach ($students as $student) {
                    if ($student['team']['team_id'] == $teams[$j]['team_id']) {
                        array_push($temp, $student);
                    }
                }
                // Записали студентов из команды отшельников в другие команды
                foreach ($singles as $i => $i)
                {
                    $is_empty = true;
                    //Проверяем не занято ли место в команде с такой же ролью
                    foreach ($temp as $index => $index) {
                        if ($temp[$index]['role']['role_id'] == $singles[$i]['role']['role_id']) {
                            $is_empty = false;
                        }
                    }
                    if ($is_empty == true)
                    {
                        array_push($temp, $singles[$i]);
                        $user_team = new UserTeam();
                        $user_team->team_id = $teams[$j]['team_id'];
                        $user_team->user_id = $singles[$i]['id'];
                        $user_team->created_at = date("Y-m-d H:i:s");
                        $user_team->save();
                        unset($singles[$i]);
                    }
                }
            }
            else {
                //Если кто-то остался в отшельниках, сортируем и возвращаем их
                if (!empty($singles))
                {
                    $temp = $singles;
                }
            }
            usort($temp, function ($a, $b) {
                return ($a['role']['role_id'] - $b['role']['role_id']);
            });
            $teams[$j]['students'] = $temp;
        }
        return $teams;
    }


    public function getStudent($id)
    {
        $user = User::findOne(['id'=>$id]);
        if ($user == null) {
            return (['message'=> 'Неверный идентификатор пользователя']);
        }
        $student = (new Query())
            ->select(['user.id', 'user.fio', 'user.email', 'user.phone', 'user.age', 'user.experience','work_status','user.study_place', 'user.period_start', 'user.period_finish','user.total_point'])
            ->from('{{user}}')
            ->where(['public.user.id' => $id])
            ->one();
        $student['role'] = $this->getLastRole($id);
        $student['stage1'] = (new UserRole())->getCountTotalPointsForFirstStage($student['id']);
        $student['stage2'] = $this->getStageResult($user,2);
        $student['stage3'] = (new UserRole())->getCountTotalPointsForSecondStage($student['id']);
//        $student['team'] = $this->getLastTeam($id);
        return $student;
    }

    public function getStageResult($user,$stage)
    {
        //Здесь будет функция по подсчету за 2-ой этап
        return 0;
    }

    public function getHermits($sort,$filter)
    {
        $role_name = Role::findOne(['name' => $filter]);
        if ($role_name == null && $filter != "0" && $filter!="")
        {
            return (['message'=>'Вы ввели неверный фильтр']);
        }
        $sorting = array();
        array_push($sorting,"1",'2','3','total','');
        if (!in_array($sort,$sorting))
        {
            return (['message'=>'Укажите верную сортировку']);
        }

        $students = (new Query())
            ->select(['user.id', 'user.total_point', 'user.study_place'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->andWhere(['public.user.is_del'=>false])
            ->orderBy('user.total_point DESC')
            ->all();

//        return count($students);
        $temp = array();
        foreach ($students as $i => $i) {
            $students[$i]['fio'] = $this->getFio($students[$i]['id']);
            $students[$i]['role'] = $this->getLastRole($students[$i]['id']);
            $students[$i]['stage1'] = (new UserRole())->getCountTotalPointsForFirstStage($students[$i]['id']);
            $students[$i]['stage3'] = (new UserRole())->getCountTotalPointsForSecondStage($students[$i]['id']);
            $team = $this->getLastTeam($students[$i]['id']);

            if ($team['team_id'] == Team::SINGLES_id) {
                if ($filter != null && $filter != '0') {
                    if ($students[$i]['role']['name'] == $filter) {
                        array_push($temp, $students[$i]);
                    }
                } else {
                    array_push($temp, $students[$i]);
                }
            }
        }
        return $this->sortStudents($temp,$sort);
    }

    protected function sortStudents($students,$stage)
    {
        //Сортируем команду по баллам первого этапа
        //Сортировка по 1-ой группе тестов
        if ($stage == 1)
        {
            usort($students, function ($a, $b) {
                return ($b['stage1'] - $a['stage1']);
            });
        }
        //Сортируем команду по баллам второго тапа
//        else if ($stage == 2)
//        {
//            usort($students, function ($a, $b) {
//                return ($b['$stage2'] - $a['$stage2']);
//            });
//        }
        //Сортируем команду по баллам третьего этапа
        else if ($stage == 3)
        {
            usort($students, function ($a, $b) {
                return ($b['stage3'] - $a['stage3']);
            });
        }
        return $students;
    }

    public function getCountLead()
    {
        return (['count'=> count($leads = User::find()
            ->where(['user.status' => User::STATUS_LEAD])
            ->andWhere(['user.is_send' => false])
            ->andWhere(['user.is_del' => false])
            ->all())]);
    }

    public function SendMails()
    {
        $leads = User::find()
            ->where(['user.status' => User::STATUS_LEAD])
            ->andWhere(['user.is_send' => false])
            ->andWhere(['user.is_del' => false])
            ->all();

        $errors = array();
        foreach ($leads as $lead) {
            if (!$this->sendEmail($lead)) {
                $errors = [
                    'message' => 'Сообщение пользователю с ' . $lead['email'] . ' почтой не отправилось.'
                ];
                break;
            }
            else {
                $student = User::findOne(['id'=> $lead['id']]);
                $student->is_send = true;
                $student->save();
            }
        }
        if ($errors != null)
            return $errors;
        else {
            return (['massage' => 'Сообщения были отправлены']);
        }
    }

    /**
     * Sends confirmation email to user
     * @param User $user user model to with email should be send
     * @return bool whether the email was sent
     */

    protected function sendEmail($user)
    {
        $message = Yii::$app->mailer->compose(
            ['html' => 'emailVerify-html', 'text' => 'emailVerify-text'],
            ['user' => $user]
        );
        $message->getSwiftMessage()->getHeaders()->addTextHeader('List-Unsubscribe', 'erdem.testuser@yandex.ru');
        return $message
            ->setFrom([Yii::$app->params['email'] => 'SmartWorld Academy'])
            ->setTo($user->email)
            ->setSubject('Регистрация в проекте SmartWorld Academy')
            ->send();
    }

}