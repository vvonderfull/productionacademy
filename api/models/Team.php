<?php

namespace api\models;

use api\models\Student;
use Yii;
use common\models\User;
use yii\db\ActiveRecord;
use yii\db\Query;

/**
 * This is the model class for table "team".
 *
 * @property int $id
 * @property string|null $name
 * @property string|null $project
 * @property bool $inSet
 * @property int|null $creator_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property User $creator
 * @property UserTeam[] $userTeams
 */
class Team extends ActiveRecord
{
    const SINGLES_id = 21;

    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['inSet','isDel'], 'boolean', 'message'=> 'Значение должно быть либо 0, либо 1'],
            [['creator_id'], 'default', 'value' => null],
            [['creator_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'project'], 'string', 'max' => 255],
            [['creator_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['creator_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Название команды',
            'project' => 'Проект Команды',
            'inSet' => 'Сформированная команда',
            'isDel' => 'Удаленная команда',
            'creator_id' => 'Создатель команды',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getCreator()
    {
        return $this->hasOne(User::className(), ['id' => 'creator_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */

    public function getUserTeams()
    {
        return $this->hasMany(UserTeam::className(), ['team_id' => 'id']);
    }

    public function ChangeStatusTeam($in_set)
    {
        $students = (new Query())
            ->select(['user.id','user.total_point','user.study_place'])
            ->from('{{user}}')
            ->where(['public.user.status' => User::STATUS_STUDENT])
            ->all();
        $count = 0;
        foreach ($students as $i => $i) {
            $team = (new Student())->getLastTeam($students[$i]['id']);
            if ($team['team_id'] == $this->id) {
                $count++;
            }
        }
        if ($count == 0)
        {
            return (['message'=>'Пустая команда не может быть сформирована']);
        }

        $this->inSet = $in_set;
        if (!$this->save())
        {
            return ['message' => $this->getErrors()];
        }
        if ($in_set == false)
        {
            return ['message' => 'Команда рассформирована!'];
        }
        elseif ($in_set == true)
        {
            return ['message' => 'Команда сформирована!'];
        }
    }

    public function ChangeTeamName($team_id)
    {
        $team = Team::findOne(['id'=>$team_id]);
        if ($team == null) {
            return (['message'=> 'Такой команды не существует']);
        }
        else {
            if ($this->project != null)
            {
                $team->project = $this->project;
            }
            $team->name = $this->name;
            $team->updated_at = date("Y-m-d H:i:s");
            if (!$team->save())
            {
                return ['message' => $team->getErrors()];
            }
            else {
                return ['message' => 'Изменения сохранены'];
            }
        }
    }

    public function DisbandTeam()
    {
        $students = UserTeam::find()->where(['team_id' => $this->id])->all();
        if ($this->id == 21){
            return ['message' => 'Эту команду нельзя забанить'];
        }
        foreach ($students as $item)
        {
            $student = UserTeam::findOne(['user_id' =>$item['user_id']]);
            $student->team_id = 21;
            $student->save();
        }
        $this->inSet = false;
        $this->save();
        return ['message' => 'Команда забанена!'];
    }

}
