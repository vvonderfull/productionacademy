<?php

namespace api\models;

use api\models\Question;
use api\models\Role;
use api\models\TestQuestion;
use api\models\UserAnswer;
use api\models\UserRole;
use common\models\User;
use mysql_xdevapi\Result;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "test".
 *
 * @property int $id
 * @property int $role_id
 * @property int $max_points
 * @property int $stage
 * @property int $time_to_test_in_minutes
 * @property string|null $path
 * @property string|null $label
 * @property string|null $description
 * @property string|null $header
 *
 * @property Role $role
 * @property TestQuestion[] $testQuestions
 * @property Question[] $questions
 */
class Test extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'test';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['role_id'], 'required'],
            [['role_id'], 'default', 'value' => null],
            [['role_id'], 'integer'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'role_id' => 'Role ID',
        ];
    }

    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    public function getTestQuestions()
    {
        return $this->hasMany(TestQuestion::className(), ['test_id' => 'id']);
    }

    public function getQuestions()
    {
        return $this->hasMany(Question::className(), ['id' => 'question_id'])->viaTable('test_question', ['test_id' => 'id']);
    }

    public function startTest($user)
    {
        $user_role = new UserRole();
        $user_role = $user_role->getLastUserRole($user->id);
        $user_role->test_date = date("Y-m-d H:i:s");
        if ($user_role->save()) {
            return (['message'=>'Тест успешно начался']);
        }
        return (['message'=>'Не удалось начать тест']);
    }

    public function getAllTests($user)
    {
        $user_role_id = $user->getLastRoleId();
        if ($user_role_id == null)
        {
            return (['message'=>'Данный пользователь не имеет роли']);
        }
        else {
            $tests = array();
            foreach (range(1, 3) as $stage) {
                $test = (new Query())
                    ->select(['test.id', 'test.path', 'test.max_points', 'time_to_test_in_minutes','test.label','test.header','test.description'])
                    ->from('{{test}}')
                    ->join('LEFT JOIN', '{{public.test_role}}', 'test.id = test_role.test_id')
                    ->where(['test.stage' => $stage])
                    ->andWhere(['test_role.role_id' => $user_role_id])
                    ->all();
                $tests[$stage - 1]['stage'] = $stage;
                $tests[$stage - 1]['tests'] = $test;
            }
            return $tests;
        }
    }

    public function setResultToTest($user,$test_id,$points)
    {
        $user_role_id = $user->getLastRoleId();

        $test = Test::findOne(['id' => $test_id]);
        if ($test == null) {
            return (['message' => 'Вы указали неверный тест']);
        }
        If ($test->max_points == null) {
            if ($points > 100) {
                return (['message' => 'Баллы не могут превышать максимальный']);
            }
        }
        else {
            if ($points > $test->max_points) {
                return (['message' => 'Баллы не могут превышать максимальный']);
            }
        }

        $role = new UserRole();
        $user_role = $role->getUserRole($user->id,$test_id);
        if ($user_role != null)
        {
            $user_role->test_date = date("Y-m-d H:i:s");
            $user_role->points = $points;

            if ($user_role->save()) {
                return (['message'=>'Результаты теста успешно сохранены']);
            }
            else {
                return $user_role->getErrors();
            }
        }
        else {
            $role->user_id = $user->id;
            $role->role_id = $user_role_id;
            $role->test_id = $test_id;
            $role->test_date = date("Y-m-d H:i:s");
            $role->points = $points;

            if ($role->save()) {
                return (['message' => 'Результаты теста успешно сохранены']);
            } else {
                return $role->getErrors();
            }
        }
    }

    public function getTestsForSecondStage($user_id)
    {
        $user = User::findOne(['id'=>$user_id]);
        $user_role_id = $user->getLastRoleId();
        if ($user_role_id == null)
        {
            return (['message'=>'Данный пользователь не имеет роли']);
        }
        else {
            return $test = (new Query())
                ->select(['test.id', 'test.path', 'test.max_points', 'time_to_test_in_minutes'])
                ->from('{{test}}')
                ->join('LEFT JOIN', '{{public.test_role}}', 'test.id = test_role.test_id')
                ->where(['test.stage' => 3])
                ->andWhere(['test_role.role_id' => $user_role_id])
                ->all();
        }
    }

    public function getTestResults($user)
    {
        $user_role = new UserRole();
        $temp = array();

        foreach (range(7,11) as $index)
        {
            $obj['result'] = $user_role->getCountPoints($user->id,$index);
            $obj['test_id'] = $index;
            array_push($temp,$obj);
        }

        $secondTest = $this->getTestsForSecondStage($user->id);
        foreach ($secondTest as $i => $i)
        {
            $obj['result'] = $user_role->getCountPoints($user->id,$secondTest[$i]['id']);
            $obj['test_id'] = $secondTest[$i]['id'];
            array_push($temp,$obj);
        }
        return $temp;
    }

    public function finishTests($user)
    {
        $user_role = new UserRole();
        $resultStage1 = $user_role->getCountTotalPointsForFirstStage($user->id);
        $resultStage2 = $user_role->getCountTotalPointsForSecondStage($user->id);
        $result = $resultStage1 + $resultStage2;
        $user->total_point = $result;
        $user->status = User::STATUS_STUDENT;

        if (!$user->save())
        {
            return $user->getErrors();
        }
        else {
            if ($user->getTeam()) {
                return (['message' => 'Вы успешно прошли все тесты']);
            }
        }
    }

    // Реализованный функционал тестов
    public function getAllQuestions($user)
    {
        $user_role_id = $user->getLastRoleId();
//        //Вывод всех вопросов
        $questions = (new Query())
            ->select(['question.id AS question_id', 'question.text AS question_text'])
            ->from('{{question}}')
            ->join('LEFT JOIN', '{{public.test_question}}', 'public.test_question.question_id = question.id')
            ->join('LEFT JOIN', '{{public.test}}', 'public.test.id = public.test_question.test_id')
            ->where(['public.test.role_id' => $user_role_id])
            ->orderBy('question.id')
            ->all();

        //Вывод всех ответов
        foreach ($questions as $index => $index) {
            $answers = (new Query())
                ->select(['answer.id', 'answer.text'])
                ->from('{{answer}}')
                ->where(['public.answer.question_id' => $questions[$index]['question_id']])
                ->all();
            $questions[$index]['answers'] = $answers;
        }
        return $questions;
    }

    public function calculateResult($user)
    {
        $result = $this->calculateRightAnswers($user);
        if ($user->changeTotalResult($result)) {
            $this->deleteUserAnswers($user);
            $user->getTeam();
            return ['message' => 'Все ответы записаны! Результаты подсчитаны! Команда присвоена'];
        }
        else {
            return ['message' => 'Неизвестная ошибка'];
        }
    }

    protected function calculateRightAnswers($user)
    {
        //Ответы пользователя
        $user_answers = (new Query())
            ->select(['user_answer.id', 'user_id', 'question_id', 'answer_id', 'question.right_answer_id'])
            ->from('user_answer')
            ->join('RIGHT JOIN', '{{public.question}}', 'public.question.id = public.user_answer.question_id')
            ->andwhere(['user_answer.user_id' => $user->id])
            ->all();

        // Посчитаем количество правильных ответов
        $right_answers = 0;
        foreach ($user_answers as $index => $index) {
            if ($user_answers[$index]['answer_id'] == $user_answers[$index]['right_answer_id']) {
                $right_answers++;
            }
        }

        $role_id = $user->getLastRoleId();
        //Подсчет вопросов
        $question_count = (new Query())
            ->select('question.id')
            ->from('question')
            ->join('JOIN', '{{public.test_question}}', 'test_question.question_id = question.id')
            ->join('JOIN', '{{public.test}}', 'test.id = test_question.test_id')
            ->where(['test.role_id' => $role_id])
            ->count();

        return $result = round(($right_answers / $question_count) * 100);
    }


    protected function deleteUserAnswers($user)
    {
        $user_answers = UserAnswer::deleteAll(['user_id'=>$user->id]);
    }

}
