<?php

namespace api\models;

use Yii;
use common\models\User;
use yii\db\Query;

/**
 * This is the model class for table "user_answer".
 *
 * @property int $id
 * @property int $user_id
 * @property int $answer_id
 * @property int $question_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Answer $answer
 * @property Question $question
 * @property User $user
 */
class UserAnswer extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_answer';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'answer_id', 'question_id'], 'required'],
            [['user_id', 'answer_id', 'question_id'], 'default', 'value' => null],
            [['user_id', 'answer_id', 'question_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['answer_id'], 'exist', 'skipOnError' => true, 'targetClass' => Answer::className(), 'targetAttribute' => ['answer_id' => 'id']],
            [['question_id'], 'exist', 'skipOnError' => true, 'targetClass' => Question::className(), 'targetAttribute' => ['question_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'User ID',
            'answer_id' => 'ID ответа',
            'question_id' => 'ID вопроса',
            'created_at' => 'Дата создания',
            'updated_at' => 'Дата обновления',
        ];
    }

//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getAnswer()
//    {
//        return $this->hasOne(Answer::className(), ['id' => 'answer_id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getQuestion()
//    {
//        return $this->hasOne(Question::className(), ['id' => 'question_id']);
//    }
//
//    /**
//     * @return \yii\db\ActiveQuery
//     */
//    public function getUser()
//    {
//        return $this->hasOne(User::className(), ['id' => 'user_id']);
//    }

    public function setAnswer($user)
    {
        if ($this->checkAnswer($user) == "the same answer") {
            return (['message'=> 'Вы уже отвечали так на этот вопрос']);
        }
        else if ($this->checkAnswer($user) == "update answer")
        {
            $user_answer = $this->getLastAnswer($user->id,$this->question_id);
            $answer = UserAnswer::findOne(['id'=>$user_answer['user_answer_id']]);
            $answer->answer_id = (integer) $this->answer_id;
            $answer->updated_at = date("Y-m-d H:i:s");
            if ($answer->save()) {
                return ['message' => 'Ответ успешно сохранён!'];
            }
            return ['message' => $answer->getErrors()];
        }
        else {
            $this->user_id = $user->id;
            $this->created_at = date("Y-m-d H:i:s");
            $this->updated_at = date("Y-m-d H:i:s");
            if ($this->save()) {
                return ['message' => 'Ответ успешно сохранён!'];
            }
            return ['message' => $this->getErrors()];
        }
    }

    public function getAnswer($user_id,$question_id)
    {
        $answer = $this->getLastAnswer($user_id,$question_id);
        if ($answer == null) {
          return (['message'=>'Пользователь еще не отвечал на этот вопрос']);
        }
        else return $answer;
    }

    protected function getLastAnswer($user_id,$question_id)
    {
        return $answer = (new Query())
            ->select(['user_answer.id AS user_answer_id', 'answer.id','answer.text','created_at'])
            ->from('{{public.answer}}')
            ->join('LEFT JOIN', '{{public.user_answer}}','public.user_answer.answer_id = public.answer.id')
            ->where(['user_answer.user_id'=>$user_id])
            ->andWhere(['user_answer.question_id'=>$question_id])
            ->orderBy('user_answer.created_at DESC')
            ->limit(1)
            ->one();
    }

    protected function checkAnswer($user)
    {
        $user_answer = $this->getLastAnswer($user->id,$this->question_id);
        if ($user_answer == null)
        {
            return "new answer";
        }
        else if ($user_answer['id'] != $this->answer_id) {
            return "update answer";
        }
        return "the same answer";
    }
}
