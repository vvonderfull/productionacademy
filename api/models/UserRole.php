<?php

namespace api\models;

use api\models\SignUpFullForm;
use api\models\Student;
use common\models\User;
use Yii;
use yii\db\Query;

/**
 * This is the model class for table "user_role".
 *
 * @property int $id
 * @property int $user_id
 * @property int $role_id
 * @property int $test_id
 * @property int|null $points
 * @property string|null $test_date
 *
 * @property Role $role
 * @property User $user
 */
class UserRole extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_role';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'role_id'], 'required'],
            [['user_id', 'role_id', 'points'], 'default', 'value' => null],
            [['user_id', 'role_id', 'points'], 'integer'],
            [['test_date'], 'safe'],
            [['role_id'], 'exist', 'skipOnError' => true, 'targetClass' => Role::className(), 'targetAttribute' => ['role_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID пользователя',
            'role_id' => 'ID роли',
            'points' => 'Очки теста',
            'test_date' => 'Дата теста',
        ];
    }

    public function getLastUserRole($user_id)
    {
        $student = new Student();
        $temp = $student->getLastRole($user_id);
        return UserRole::findOne(['id'=>$temp['id']]);
    }

    public function getCountPoints($user_id,$test_id)
    {
        $user_role = $this->getUserRole($user_id,$test_id);
        if ($user_role == null) {
            return 0;
        }
        else {
            return $user_role['points'];
        }
    }

    public function getCountTotalPointsForFirstStage($user_id)
    {
        $ustupok = $this->getCountPoints($user_id, 7);
        $sotrud = $this->getCountPoints($user_id, 9);
        $kompromis = $this->getCountPoints($user_id, 10);
        $protivoborstvo = $this->getCountPoints($user_id, 8);
        $izbeganie = $this->getCountPoints($user_id, 11);

        $total = ($kompromis + $sotrud + $ustupok - $izbeganie - $protivoborstvo) / 35 * 100;
        if ($total < 0) {
            return 0;
        } else {
            return round($total);
        }
    }

    public function getCountTotalPointsForSecondStage($user_id)
    {
        $sum = 0;
        $secondTest = (new Test())->getTestsForSecondStage($user_id);
        if (array_key_exists('message',$secondTest)) {
            return 0;
        }
        $count = count($secondTest);
        if ($count == 0) {
            return 0;
        }
        else {
            foreach ($secondTest as $j => $j) {
                    if ($secondTest[$j]['max_points'] != null) {
                        $point = $this->getCountPoints($user_id, $secondTest[$j]['id']);
                        $sum += ($point / $secondTest[$j]['max_points'] * 100);
                    }
                $sum += $this->getCountPoints($user_id, $secondTest[$j]['id']);
            }
            return round($sum / $count);
        }
    }

    public function getUserRole($user_id,$test_id)
    {
        $user_roles = (new Query())
            ->select(['user_role.id','user_role.role_id','user_role.test_date','user_role.points'])
            ->from('{{public.role}}')
            ->join('LEFT JOIN', 'public.user_role', 'public.role.id = public.user_role.role_id')
            ->where(['user_role.user_id'=>$user_id])
            ->andWhere(['user_role.test_id'=>$test_id])
            ->orderBy('user_role.test_date DESC')
            ->all();

        foreach ($user_roles as $index => $index)
        {
            $date = $user_roles[$index]['test_date'];
            $now = date("Y-m-d H:i:s");
            $interval = (new SignUpFullForm())->dateDifference($now, $date);
            if ($interval < 30)
            {
                return UserRole::findOne(['id'=>$user_roles[$index]['id']]);
            }
        }
        return null;
    }

    public function ChangeTotalResult($result)
    {
        $this->points = $result;
        if ($this->save())
            return (['message' => 'Тест успешно пройден']);
        return $this->getErrors();
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getRole()
    {
        return $this->hasOne(Role::className(), ['id' => 'role_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }
}
