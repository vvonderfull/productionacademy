<?php

namespace api\models;
use api\models\Student;
use common\models\User;
use Yii;

/**
 * This is the model class for table "user_team".
 *
 * @property int $id
 * @property int $user_id
 * @property int $team_id
 * @property string|null $created_at
 * @property string|null $updated_at
 *
 * @property Team $team
 * @property User $user
 */
class UserTeam extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'user_team';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['user_id', 'team_id'], 'required'],
            [['user_id', 'team_id'], 'default', 'value' => null],
            [['user_id', 'team_id'], 'integer'],
            [['created_at'], 'safe'],
            [['team_id'], 'exist', 'skipOnError' => true, 'targetClass' => Team::className(), 'targetAttribute' => ['team_id' => 'id']],
            [['user_id'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['user_id' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'user_id' => 'ID команды',
            'team_id' => 'ID команды',
            'created_at' => 'Дата создания команды',
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getTeam()
    {
        return $this->hasOne(Team::className(), ['id' => 'team_id']);
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getUser()
    {
        return $this->hasOne(User::className(), ['id' => 'user_id']);
    }

    public function ChangeTeam($user_id,$team_id)
    {
        $team = Team::findOne(['id'=> $team_id]);
        $user = User::findOne(['id'=>$user_id]);
        if ($team == null || $user == null) {
            return ['message' => 'Несуществующая команда или пользователь'];
        }
        else if ($this->getUserTeam($user_id) == $team_id) {
            return (['message' => 'Пользователь уже находится в этой команде']);
        }
        else {
            $this->team_id = $team_id;
            $this->user_id = $user_id;
            $this->created_at = date("Y-m-d H:i:s");
            if (!$this->save()) {
                return (['message' => $this->getErrors()]);
            } else {
                return (['message' => 'Пользователь с id = ' . $this->user_id . ' теперь в команде с id = ' . $this->team_id]);
            }
        }
    }
    protected function getUserTeam($user_id)
    {
        $student = new Student();
        return $student->getLastTeam($user_id)['team_id'];
    }
}
