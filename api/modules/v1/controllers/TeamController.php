<?php


namespace api\modules\v1\controllers;


use api\models\CheckAccess;
use api\models\Student;
use api\models\Team;
use api\models\UserTeam;
use Yii;
use yii\filters\auth\HttpBearerAuth;

class TeamController extends CommonController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        # Наследуем поведение родителя
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'change-user-team',
                'disband-team',
                'change-status-team',
                'change-team-name',
                'form-teams',
            ]
        ];
        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'change-user-team' => ['POST', 'OPTIONS'],
            'change-status-team' => ['POST', 'OPTIONS'],
            'disband-team' => ['POST', 'OPTIONS'],
            'change-team-name' => ['POST', 'OPTIONS'],
            'form-teams' => ['POST', 'OPTIONS'],
        ];
    }

    public function actionChangeUserTeam()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $user_id = Yii::$app->getRequest()->getbodyParam('user_id');
            $team_id = Yii::$app->getRequest()->getbodyParam('team_id');

            $team = new UserTeam();
            return $team->ChangeTeam($user_id, $team_id);
        }
    }

    public function actionFormTeams()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $student = new Student();
            return $student->FormStudents();
        }
    }

    public function actionChangeStatusTeam()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $team_id = Yii::$app->getRequest()->getbodyParam('team_id');
            $form = Yii::$app->getRequest()->getBodyParam('form');
            $team = Team::findOne(['id' => $team_id]);
            if ($team == null || $team_id == Team::SINGLES_id) {
                return ['message' => 'Вы ввели неверную команду!'];
            }
            return $team->ChangeStatusTeam($form);
        }
    }

    public function actionChangeTeamName()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $team_id = Yii::$app->getRequest()->getBodyParam('team_id');
            $model = new Team();
            $model->load(Yii::$app->getRequest()->getBodyParams(), '');
            return $model->changeTeamName($team_id);
        }
    }


    public function actionDisbandTeam()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $team_id = Yii::$app->getRequest()->getbodyParam('team_id');
            $team = Team::findOne(['id'=> $team_id]);
            return $team->DisbandTeam();
        }
    }


}