<?php
namespace api\modules\v1\controllers;

use api\models\Student;
use api\models\Test;
use api\models\UserAnswer;
use api\models\UserRole;
use common\models\User;
use Yii;
use yii\db\Query;
use yii\filters\auth\HttpBearerAuth;
use api\models\CheckAccess;

class TestController extends CommonController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        # Наследуем поведение родителя
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'get-all-questions',
                'set-answer',
                'calculate-result',
                'get-answer',
                'start-test',
                'get-all-tests',
                'set-result-to-test',
                'get-test-results',
                'finish-tests',
            ]
        ];
        return $behaviors;
    }

    protected function verbs()
    {
        return [
            'get-all-questions' => ['GET', 'OPTIONS'],
            'set-answer' => ['POST', 'OPTIONS'],
            'calculate-result' => ['POST', 'OPTIONS'],
            'get-answer' => ['GET', 'OPTIONS'],
            'start-test' => ['POST', 'OPTIONS'],
            'get-all-tests' => ['GET', 'OPTIONS'],
            'set-result-to-test' => ['POST', 'OPTIONS'],
            'get-test-results' => ['GET', 'OPTIONS'],
            'finish-tests' => ['POST','OPTIONS'],
        ];
    }

    public function actionStartTest()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            return $test->startTest($user);
        }
    }

    public function actionGetAllTests()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            return $test->getAllTests($user);
        }
    }

    public function actionSetResultToTest()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            $test_id = Yii::$app->getRequest()->getBodyParam('test_id');
            $points = Yii::$app->getRequest()->getBodyParam('points');
            return $test->setResultToTest($user,$test_id,$points);
        }
    }

    public function actionGetTestResults()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'' );
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            return $test->getTestResults($user);
        }
    }

    public function actionFinishTests()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            return $test->finishTests($user);
        }
    }

    public function actionGetAllQuestions()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $test = new Test();
            return $test->getAllQuestions($user);
        }
    }


    public function actionSetAnswer()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $model = new UserAnswer();
            $model->load(Yii::$app->getRequest()->getBodyParams(), '');
            return $model->setAnswer($user);
        }
    }

    public function actionGetAnswer()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $model = new UserAnswer();
            $question_id = Yii::$app->getRequest()->getQueryParam('question_id');
            return $model->getAnswer($user->id, $question_id);
        }
    }


    public function actionCalculateResult()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $model = new Test();
            return $model->CalculateResult($user);
        }
    }

}
