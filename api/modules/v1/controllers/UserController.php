<?php

namespace api\modules\v1\controllers;

use api\models\CheckAccess;
use api\models\ExcelForm;
use api\models\LoginForm;
use api\models\SignUpForm;
use api\models\SignUpFullForm;
use api\models\Student;
use api\models\Token;
use api\models\UserTeam;
use api\models\Team;
use common\models\User;
use Yii;
use yii\filters\auth\HttpBearerAuth;
use yii\filters\ContentNegotiator;
use yii\web\Response;

class UserController extends CommonController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();
        # Наследуем поведение родителя
        $behaviors['authenticator'] = [
            'class' => HttpBearerAuth::className(),
            'only' => [
                'get-user',
                'get-all-students',
                'get-all-students-in-set',
                'send-mails',
                'get-hermits',
                'change-path-to-pruff',
                'get-link-to-drive',
                'get-excel-roles',
                'get-excel-teams',
            ]
        ];
        return $behaviors;
    }


    public function actions()
    {
        return [];
    }

    protected function verbs()
    {
        return [
            'index' => ['GET', 'OPTIONS'],
            'login' => ['POST', 'OPTIONS'],
            'sign-up' => ['POST', 'OPTIONS'],
            'get-all-students' => ['GET', 'OPTIONS'],
            'get-all-students-in-set' => ['GET', 'OPTIONS'],
            'send-token' => ['POST', 'OPTIONS'],
            'send-mails' => ['POST', 'OPTIONS'],
            'sign-up-second' => ['GET', 'POST', 'OPTIONS'],
            'get-count-lead'=> ['GET', 'OPTIONS'],
            'get-hermits' => ['GET','OPTIONS'],
            'change-path-to-pruff' => ['POST','OPTIONS'],
            'get-link-to-drive' => ['GET', 'OPTIONS'],
            'get-excel-teams' => ['GET', 'OPTIONS'],
            'get-excel-roles'=> ['GET', 'OPTIONS']
        ];
    }

    public function actionIndex()
    {
        return 'Ваша api работает!';
    }

    public function actionLogin()
    {
        $model = new LoginForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        return $model->login();
    }

    public function actionSignUp()
    {
        $model = new SignUpForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        return $model->signup();
    }

    public function actionSignUpSecond()
    {
        //TODO исправить на bearer token
//        $token = Yii::$app->getRequest()->getBodyParam('token');

        $model = new SignUpFullForm();
        $model->load(Yii::$app->getRequest()->getBodyParams(), '');
        return $model->signUp();
    }

    public function actionGetLinkToDrive()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            return $user->getLinkToDrive();
        }
    }

    public function actionChangePathToPruff()
    {
        $access = new CheckAccess();
        $user = $access->checkToken(true,'');
        if ($user == null) {
            return ['message' => 'нет прав!'];
        } else {
            $link = Yii::$app->getRequest()->getBodyParam('link');
            return $user->changePathToPruff($link);
        }
    }

    public function actionSendMails()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $student = new Student();
            return $student->sendMails();
        }
    }

    public function actionGetAllStudents()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $students = new Student();
            return ($students->getAllStudents(false));
        }
    }

    public function actionGetHermits()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $students = new Student();
            $sort = Yii::$app->getRequest()->getQueryParam('sort');
            $filter = Yii::$app->getRequest()->getQueryParam('filter');
            return ($students->getHermits($sort,$filter));
        }
    }

    public function actionGetAllStudentsInSet()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $students = new Student();
            return ($students->getAllStudents(true));
        }
    }

    public function actionGetUser()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $id = Yii::$app->getRequest()->getQueryParam('id');
            $students = new Student();
            return ($students->getStudent($id));
        }
    }

    public function actionGetExcelRoles()
    {
        $token = Yii::$app->getRequest()->getQueryParam('token');
        $access = new CheckAccess();

        if (!$access->checkRules($token)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $model = new ExcelForm();
            return ($model->formExcelOnRoles());
        }
    }

    public function actionGetExcelTeams()
    {
        $token = Yii::$app->getRequest()->getQueryParam('token');
        $access = new CheckAccess();

        if (!$access->checkRules($token)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $model = new ExcelForm();
            return ($model->FormExcelOnTeams());
        }
    }

    public function actionSendToken()
    {
        $token = Yii::$app->getRequest()->getbodyParam('token');;
        $user_token = Token::findOne(['token'=>$token]);
        if ($user_token == null)
        {
            return (['message'=> 'Токен не валидный']);
        }
        $user = User::findOne(['id'=> $user_token->user_id]);
        if ($user == null)
            return ['message' => 'Пользователь не найден'];
        else {
            return $user->getAttributes(['phone','fio','email']);
        }
    }

    public function actionGetCountLead()
    {
        $access = new CheckAccess();
        if (!$access->checkRules(null)) {
            return ['message' => 'нет прав!'];
        }
        else {
            $students = new Student();
            return ($students->getCountLead());
        }
    }
}