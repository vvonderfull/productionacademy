<?php
use yii\helpers\Html;
use yii\helpers\Url;

/* @var $this yii\web\View */
/* @var $user common\models\User */

//$verifyLink = \yii\helpers\Url::to('http://localhost:8080/studentRegistration?token='.$user->getVerificationToken());
//$verifyLink = (['http://localhost:8081/studentRegistration', 'token' => $user->getVerificationToken()]);


$verifyLink = Url::to('http://localhost:8080/studentRegistration?token=' . $user->getVerificationToken());
$anotherLink = Url::to('http://mentor.student.smartworld.team:2280/studentRegistration?token=' . $user->getVerificationToken());
?>

<div class="verify-email">
    <p>Приветствуем тебя, <?= Html::encode($user->fio) ?>!</p>

    <p>
        Мы знаем, что ты хочешь войти в IT, и свой путь ты начал, оставив заявку на нашем сайте Smart World Academy.
    </p>
    <p>
        Сейчас тебя ждет следующий этап - входное тестирование. Для его прохождения необходимо зарегистрироваться на нашем сервисе.
    </p>

    <p>Ссылка на регистрацию:</p>
    <p><?= Html::a(Html::encode($anotherLink), $anotherLink) ?></p>

    <p>При регистрации тебе необходимо выбрать роль в команде. Ее нельзя изменить до начала Академии. Но по ходу обучения, если захочешь, можно будет поменять роль.</p>

    <p>Все дальнейшие инструкции будут уже после регистрации.</p>

    <p>В случае возникновения трудностей можешь связаться с нами написав на почту: academy@smartworld.team </p>

    <p>Все зависит только от тебя! </p>

</div>
