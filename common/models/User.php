<?php

namespace common\models;

use api\models\Student;
use api\models\Token;
use api\models\Role;
use api\models\Team;
use api\models\UserAnswer;
use api\models\UserRole;
use api\models\UserTeam;
use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\db\Query;
use yii\debug\panels\EventPanel;
use yii\web\IdentityInterface;

/**
 * User model
 *
 * @property integer $id
 * @property string $password_hash
 * @property string $password_reset_token
 * @property string $verification_token
 * @property string $email
 * @property integer $status
 * @property string $auth_key
 * @property integer $created_at
 * @property integer $updated_at
 * @property string $password write-only password
 * @property string $role_name write-only role_name
 * @property string $role_id write-only role_id
 * @property string|null $phone
 * @property string|null $fio
 * @property int|null $age
 * @property string|null $study_place
 * @property string|null $experience
 * @property int|null $period_start
 * @property int|null $period_finish
 * @property boolean|null $work_status
 * @property boolean|null $is_send
 * @property boolean|null $is_del
 * @property string|null $link_to_drive
 *
 * @property string|null $comment
 * @property int|null $total_point
 * @property Team[] $teams
 * @property UserAnswer[] $userAnswers
 * @property UserRole[] $userRoles
 * @property UserTeam[] $userTeams
 *
 */
class User extends ActiveRecord implements IdentityInterface
{
    const SCENARIO_REGISTER = 'Signupsecond';
    const SCENARIO_LOGIN = 'login';
    const SCENARIO_SIGNUP = 'signup';
    const STATUS_DELETED = 0;
    const STATUS_INACTIVE = 9;
    const STATUS_ACTIVE = 10;
    const STATUS_LEAD = 11;
    const STATUS_STUDENT = 12;
    const STATUS_MENTOR = 13;
    const STATUS_SUPERVISOR = 14;
    const STATUS_SUPERMENTOR = 15;
    public $password;
    public $role_id;
    public $role_name;

    public static function tableName()
    {
        return '{{%user}}';
    }

    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            TimestampBehavior::className(),
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['status', 'created_at', 'updated_at', 'age', 'period_finish', 'period_start', 'total_point'], 'default', 'value' => null],
            [['status', 'created_at', 'updated_at', 'age', 'total_point'], 'integer'],
            [['work_status'], 'boolean'],
            [['role_id'], 'integer'],
            [['password','link_to_drive'], 'string'],
            [['link_to_drive'], 'url', 'message'=> 'Вы ввели неверную ссылку'],
            [['role_name'], 'string'],
            [['email', 'password_reset_token', 'password_hash', 'phone', 'fio', 'study_place', 'experience', 'comment'], 'string', 'max' => 255],
            [['password_reset_token'], 'unique'],
            ['status', 'default', 'value' => self::STATUS_INACTIVE],
            ['status', 'in', 'range' => [self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_DELETED, self::STATUS_LEAD, self::STATUS_STUDENT, self::STATUS_MENTOR, self::STATUS_SUPERVISOR, self::STATUS_SUPERMENTOR]],

            [['age'], 'integer', 'min' => 18, 'on' => self::SCENARIO_REGISTER, 'message' => 'Значение поля Возраст должен не мение 18'],
            [['age', 'role_name', 'password', 'experience'], 'required', 'on' => self::SCENARIO_REGISTER, 'message' => 'Укажите {attribute}'],
            [['password'], 'string', 'min' => 8, 'on' => self::SCENARIO_REGISTER],
            [['password'], 'string', 'max' => 30, 'on' => self::SCENARIO_REGISTER],
            [['work_status'], 'boolean', 'on' => self::SCENARIO_REGISTER],

        ];
    }
    public function attributeLabels()
    {
        return [
            'email' => 'почта',
            'password_hash' => 'хэш пароля',
            'password' => 'пароль',
            'experience' => 'опыт',
            'role_name' => 'роль',
            'comment' => 'комментарий',
            'age' => 'возраст',
            'phone' => 'номер телефон',
        ];
    }
    public function scenarios()
    {
        $scenarios = parent::scenarios();
        $scenarios['self::SCENARIO_REGISTER'] = ['age', 'password', 'email', 'role_id', 'period_start','period_finish', 'experience', 'role_name', 'comment','work_status'];
        $scenarios['self::SCENARIO_LOGIN'] = ['password', 'email', 'password_hash'];
        return $scenarios;
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    /**
     * {@inheritdoc}
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        return Token::findOne(['token' => $token]);
    }

    /**
     * Finds user by email
     *
     * @param string $email
     * @return static|null
     */
    public static function findByEmail($email)
    {
        return static::findOne(['email' => $email,]);
    }


    /**
     * Finds user by password reset token
     *
     * @param string $token password reset token
     * @return static|null
     */
    public static function findByPasswordResetToken($token)
    {
        if (!static::isPasswordResetTokenValid($token)) {
            return null;
        }

        return static::findOne([
            'password_reset_token' => $token,
            'status' => self::STATUS_ACTIVE,
        ]);
    }

    public static function findByVerificationToken($token)
    {
        $user_token = Token::findOne(['token'=>$token]);
        if ($user_token == null) {
            return null;
        }
        return User::findOne(['id' => $user_token->user_id]);
    }

    public function getLastRoleId()
    {
        $role = (new Query())
            ->select(['role_id', 'test_date'])
            ->from('user_role')
            ->where(['user_id' => $this->id])
            ->orderBy('test_date DESC')
            ->limit(1)
            ->one();
        return $role['role_id'];
    }


    /**
     * Finds out if password reset token is valid
     *
     * @param string $token password reset token
     * @return bool
     */
    public static function isPasswordResetTokenValid($token)
    {
        if (empty($token)) {
            return false;
        }

        $timestamp = (int)substr($token, strrpos($token, '_') + 1);
        $expire = Yii::$app->params['user.passwordResetTokenExpire'];
        return $timestamp + $expire >= time();
    }

    /**
     * {@inheritdoc}
     */
    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getVerificationToken()
    {
        $token = Token::findOne(['user_id'=>$this->id]);
        if ($token == null) {
            return null;
        }
        else {
            return $token->token;
        }
    }

    /**
     * {@inheritdoc}
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    /**
     * {@inheritdoc}
     */
    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    /**
     * Validates password
     *
     * @param string $password password to validate
     * @return bool if password provided is valid for current user
     */
    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * Generates password hash from password and sets it to the model
     *
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    /**
     * Generates "remember me" authentication key
     */
    public function generateAuthKey()
    {
        $this->auth_key = Yii::$app->security->generateRandomString();
    }

    /**
     * Generates new password reset token
     */
    public function generatePasswordResetToken()
    {
        $this->password_reset_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    public function generateEmailVerificationToken()
    {
        $this->verification_token = Yii::$app->security->generateRandomString() . '_' . time();
    }

    /**
     * Removes password reset token
     */
    public function removePasswordResetToken()
    {
        $this->password_reset_token = null;
    }

    protected function generateTokenToUser()
    {
        $token = new Token();
        $token->user_id = $this->id;
        $token->generateToken(time() + 3600 * 24);
        return $token->save() ? $token : false;
    }

    public function changeTotalResult($result)
    {
        $user_role = new UserRole();
        $user_role = $user_role->getLastUserRole($this->id);
        $user_role->points = $result;

        $this->total_point = $result;
        $this->status = User::STATUS_STUDENT;
        if ($this->save() && $user_role->save()) {
            return true;
        }
        return false;
    }

    public function getLinkToDrive()
    {
        return $this->link_to_drive;
    }

    public function changePathToPruff($link)
    {
        if ($link == null)
        {
            return (['message'=>'Вы ввели пустой адрес']);
        }
        $this->link_to_drive = $link;
        if (!$this->validate())
        {
            return $this->getErrors();
        }
        if ($this->save()) {
            return ['message' => 'Ссылка изменена'];
        }
    }

    public function getTeam()
    {
        $user_team = new UserTeam();
        $user_team->user_id = $this->id;
        $user_team->team_id = Team::SINGLES_id;
        $user_team->created_at = date("Y-m-d H:i:s");
        if ($user_team->save()) {
            return true;
        }
        else {
            return false;
        }
//        $role_id = $this->getLastRoleId();
//        $role_name = Role::findOne(['id'=>$role_id])->name;
//
//        $students = new Student();
//        $teams = $students->getAllStudents(false);
//
//        foreach ($teams as $team)
//        {
//            if ($team['team_id'] == Team::SINGLES_id) {
//                $user_team = new UserTeam();
//                $user_team->user_id = $this->id;
//                $user_team->team_id = $team['team_id'];
//                $user_team->created_at = date("Y-m-d H:i:s");
//                $user_team->save();
//                return true;
//            }
//            $temp = 0;
//            $students = $team['students'];
//            foreach ($students as $student) {
//                if ($student['role']['name'] == $role_name) {
//                    $temp++;
//                }
//            }
//            if ($temp == 0) {
//                $user_team = new UserTeam();
//                $user_team->user_id = $this->id;
//                $user_team->team_id = $team['team_id'];
//                $user_team->created_at = date("Y-m-d H:i:s");
//                $user_team->save();
//                return true;
//            }
//        }
    }
}
