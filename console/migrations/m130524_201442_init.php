<?php

use yii\db\Migration;

class m130524_201442_init extends Migration
{
    public function up()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            // http://stackoverflow.com/questions/766809/whats-the-difference-between-utf8-general-ci-and-utf8-unicode-ci
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }

        $this->createTable('{{%user}}', [
            //Идентификаторы
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull()->unique(),

            //Стандарт
//            'auth_key' => $this->string(32)->notNull(),
            'password_reset_token' => $this->string()->unique(),

            //Статус пользователя
            'status' => $this->smallInteger()->notNull()->defaultValue(11),

            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),

            //Новое
            'password_hash' => $this->string(),

            //Студент
            'phone' => $this-> string(),
            'fio'=>$this->string(),
            'age'=>$this->integer(),
            'study_place'=>$this->string(),
            'experience'=>$this->string(255),
            'period_start' => $this->dateTime(),
            'period_finish' => $this->dateTime(),
            'work_status'=>$this->boolean()->defaultValue(false),


            'comment'=>$this->string(),

            // Ссылка на гугл-диск с пруфами
            'link_to_drive' => $this-> string(),
            // Общий результат прохождения теста
            'total_point'=> $this->integer(),

            //Поле проверки отправки сообщения на почту
            'is_send'=> $this->boolean()->defaultValue(false),
            //Внешние ключики
//            'role_id'=>$this->integer(),
//            'team_id'=>$this->integer(),

            //Присутствует ли он в наборе
            'is_del'=> $this->boolean()->defaultValue(false),



        ], $tableOptions);
        $this->execute('alter sequence user_id_seq start 100;');
        $this->execute('alter sequence user_id_seq restart;');
    }

    public function down()
    {
        $this->dropTable('{{%user}}');
    }
}
