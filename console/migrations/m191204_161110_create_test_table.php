<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test}}`.
 */
class m191204_161110_create_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test}}', [
            'id' => $this->primaryKey(),
            //Чтобы хватило места используем тип text
            'path' => $this->text(),
            'max_points' => $this->integer(),
            // Хранение номера этапа
            'stage'=> $this->smallInteger(),
            'time_to_test_in_minutes' => $this-> integer() ->defaultValue(0),
            'label' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%test}}');
    }
}
