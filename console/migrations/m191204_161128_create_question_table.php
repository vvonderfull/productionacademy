<?php

use yii\db\Migration;
/**
 * Handles the creation of table `{{%question}}`.
 */
class m191204_161128_create_question_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%question}}', [
            'id' => $this->primaryKey(),
            'text' => $this->text()->notNull(),
            'right_answer_id' => $this->integer(),
        ]);
        $this->execute('alter sequence question_id_seq start 27;');
        $this->execute('alter sequence question_id_seq restart;');
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%question}}');
    }
}
