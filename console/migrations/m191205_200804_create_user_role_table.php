<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_role}}`.
 */
class m191205_200804_create_user_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%user_role}}', [
            'id' => $this->primaryKey(),
            'user_id'=>$this->integer()->notNull(),
            'role_id'=>$this->integer()->notNull(),
            'test_id'=>$this->integer()->notNull(),

            'points'=>$this->integer(),
            'test_date'=>$this->dateTime(),
        ]);
        $this->execute('alter sequence user_role_id_seq start 1000;');
        $this->execute('alter sequence user_role_id_seq restart;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%user_role}}');
    }
}
