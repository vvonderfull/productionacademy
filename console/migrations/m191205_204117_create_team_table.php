<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%team}}`.
 */
class m191205_204117_create_team_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%team}}', [
            'id' => $this->primaryKey(),
            'name'=>$this->string(),
            'project'=>$this->string(),

            'inSet'=>$this->boolean()->notNull()->defaultValue(false),
            'isDel'=>$this->boolean()->notNull()->defaultValue(false),

            'created_at'=>$this->dateTime(),
            'updated_at'=>$this->dateTime(),
            //Руководитель или же наставник, который создал команду
            'creator_id'=>$this->integer(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%team}}');
    }
}
