<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%user_key}}`.
 */
class m191208_135959_create_token_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%token}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer() -> notNull(),
            'token' => $this->string()->notNull()->unique(),
            'expired_at' => $this-> dateTime()->notNull(),
            ]);

        $this->execute('alter sequence token_id_seq start 100;');
        $this->execute('alter sequence token_id_seq restart;');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%token}}');
    }
}
