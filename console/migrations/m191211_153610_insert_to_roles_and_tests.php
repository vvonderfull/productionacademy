<?php

use yii\db\Migration;

/**
 * Class m191211_153610_insert_to_roles_and_tests
 */
class m191211_153610_insert_to_roles_and_tests extends Migration
{

    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%role}}',['name','id'], [['менеджер','1'],['аналитик','2'],['фронтэнд','3'],['бэкэнд','4'],['тестировщик','5'],['дизайнер','6']]);
        $this->batchInsert('{{%test}}',['id','stage'],
            [//[1,3],
                [2,2],
                [3,2],
                [4,2],
                [5,2],
            //    [6,3]
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%test}}', ['in', 'id', [2,3,4,5]]);
        $this->delete('{{%role}}', ['in', 'name', ['менеджер','аналитик','фронтэнд','бэкэнд','тестировщик','дизайнер']]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191211_153610_insert_to_roles cannot be reverted.\n";

        return false;
    }
    */
}
