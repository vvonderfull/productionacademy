<?php

use yii\db\Migration;

/**
 * Class m191213_094905_insert_to_user
 */
class m191213_095310_insert_to_user extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user}}',['id','age','created_at','comment','email','experience','fio','total_point','password_hash','study_place','period_start','period_finish','phone','status','updated_at','link_to_drive'],
                [[134345,19,1576226554,'test_com','mayemail@kek.ru','big exp','Purgen Vladimir Evgenievich', 31,'$2y$13$ypMKWieUuG0Ie8Rktp361.LJgUjxuQUOUL654XKySWhIOtwvfl/Ly','ТГУ','2019-12-28','2019-12-28','8 (800) 555-35-35',12,1576226554,'https://drive.google.com/drive/folders/1kvG8YCv8aXA-4aMxikUtIBslAX9NEbbG'],
                [134346,19,1576226555,'test_com','test@test.ru','big exp','Kargen Ivan Ivanovich', 32,'$2y$13$ypMKWieUuG0Ie8Rktp361.LJgUjxuQUOUL654XKySWhIOtwvfl/Ly','ТПУ','2019-12-28','2019-12-28','8 (800) 555-35-35',12,1576226555,'https://drive.google.com/drive/folders/1kvG8YCv8aXA-4aMxikUtIBslAX9NEbbG'],
                [134347,18,1576226556,'test_com','tested@test.ru','big exp','Иванов Владимир Владимирович', 33,'$2y$13$ypMKWieUuG0Ie8Rktp361.LJgUjxuQUOUL654XKySWhIOtwvfl/Ly','ТУСУР','2019-12-28','2019-12-28','8 (800) 555-35-35',12,1576226556,'https://drive.google.com/drive/folders/1kvG8YCv8aXA-4aMxikUtIBslAX9NEbbG']]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'id', ['134345','134346','134347']]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191213_094905_insert_to_user cannot be reverted.\n";

        return false;
    }
    */
}
