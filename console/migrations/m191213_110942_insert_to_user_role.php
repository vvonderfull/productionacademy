<?php

use yii\db\Migration;

/**
 * Class m191213_110942_insert_to_user_role
 */
class m191213_110942_insert_to_user_role extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user_role}}',['id','user_id','role_id','test_id','points','test_date'],
            [[134345,134345,4,4,31,'10-10-2010'],
                [134346,134346,2,2,32,'10-10-2012'],
                [134347,134347,3,3,33,'10-10-2011']]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%answer}}', ['in', 'id', [134345,134346,134347]]);
    }

}
