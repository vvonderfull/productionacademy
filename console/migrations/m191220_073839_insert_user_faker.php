<?php

use common\models\User;
use Faker\Factory;
use yii\db\Migration;

/**
 * Class m191220_073839_insert_user_faker
 */
class m191220_073839_insert_user_faker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        for($i = 0; $i <= 30; $i++)
        {
            $posts = [];

            $fake_pass = new User();
            $password = 'qwerty123';
            $fake_pass->setPassword($password);
            $pass = $fake_pass->password_hash;
            $fio = $faker->firstName . " " . $faker->lastName . " " .$faker->colorName;

            $rand_num = $faker->numberBetween(1,5);

            $vuz = "";
            switch ($rand_num)
            {
                case 1:
                    $vuz = "ТГУ";
                    break;
                case 2:
                    $vuz = "ТПУ";
                    break;
                case 3:
                    $vuz = "ТУСУР";
                    break;
                case 4:
                    $vuz = "ТГПУ";
                    break;
                case 5:
                    $vuz = "ТТИТ";
                    break;
            }
            $posts[] = [
                $faker->unique()->numberBetween(10,40),
                $faker->numberBetween(18, 100),
                $vuz,
                $faker->unixTime(),
                $faker->text(10),
                $faker->unique()->email,
                 //незахэшированный пароль будет в поле Опыт
                $password,
                $fio,
                $faker->numberBetween(0,100),
                $pass,
                $faker->date(),
                $faker->date(),
                $faker->phoneNumber,
                $faker->numberBetween(11,12),
                $faker->unixTime(),
                $faker->url,
            ];
            $this->batchInsert('{{%user}}',['id','age','study_place','created_at','comment','email','experience','fio','total_point','password_hash','period_start','period_finish','phone','status','updated_at','link_to_drive'],$posts);
        }
    }


    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 10; $i < 40; $i++) {
            $this->delete('{{%user}}', ['in', 'id', [$i]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191220_073839_insert_user_faker cannot be reverted.\n";

        return false;
    }
    */
}
