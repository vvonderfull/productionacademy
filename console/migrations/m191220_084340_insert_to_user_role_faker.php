<?php

use Faker\Factory;
use yii\db\Migration;
/**
 * Class m191220_084340_insert_to_user_role_faker
 */
class m191220_084340_insert_to_user_role_faker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        for ($i = 1; $i <= 30; $i++)
        {
            $numb = $faker->numberBetween(1,6);
            for ($j = 1; $j <= 5; $j++)
            {
                $posts = [];
                $posts[] = [
                    $faker->unique()->numberBetween(1,150),
                    $i+10,
                    $numb,
                    $j+6,
                    $faker->numberBetween(0,11),
                    date("Y-m-d H:i:s"),
                ];
                $this->batchInsert('{{%user_role}}',['id','user_id','role_id','test_id','points','test_date'],$posts);
            }
            switch ($numb) {
                case 2:
                    for ($j = 1; $j <= 4; $j++) {
                        $posts = [];
                        $posts[] = [
                            $faker->unique()->numberBetween(160, 280),
                            $i + 10,
                            $numb,
                            $j + 13,
                            $faker->numberBetween(0, 14),
                            date("Y-m-d H:i:s"),
                        ];
                        $this->batchInsert('{{%user_role}}', ['id', 'user_id', 'role_id', 'test_id', 'points', 'test_date'], $posts);
                    }
                    break;
                case 3:
                    for ($j = 1; $j <= 4; $j++) {
                        $posts = [];
                        $posts[] = [
                            $faker->unique()->numberBetween(280, 400),
                            $i + 10,
                            $numb,
                            $j + 17,
                            $faker->numberBetween(0, 6),
                            date("Y-m-d H:i:s"),
                        ];
                        $this->batchInsert('{{%user_role}}', ['id', 'user_id', 'role_id', 'test_id', 'points', 'test_date'], $posts);
                    }
                    break;
                case 4:
                    for ($j = 1; $j <= 2; $j++) {
                        $posts = [];
                        $posts[] = [
                            $faker->unique()->numberBetween(400, 520),
                            $i + 10,
                            $numb,
                            $j + 21,
                            $faker->numberBetween(0, 20),
                            date("Y-m-d H:i:s"),
                        ];
                        $this->batchInsert('{{%user_role}}', ['id', 'user_id', 'role_id', 'test_id', 'points', 'test_date'], $posts);
                    }
                    break;
                case 5:
                    for ($j = 1; $j <= 2; $j++) {
                        $posts = [];
                        $posts[] = [
                            $faker->unique()->numberBetween(520, 640),
                            $i + 10,
                            $numb,
                            $j + 23,
                            $faker->numberBetween(0, 15),
                            date("Y-m-d H:i:s"),
                        ];
                        $this->batchInsert('{{%user_role}}', ['id', 'user_id', 'role_id', 'test_id', 'points', 'test_date'], $posts);
                    }
                    break;
            }
        }

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 1; $i <= 640 ; $i++) {
            $this->delete('{{%user_role}}', ['in', 'id', [$i]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191220_084340_insert_to_user_role_faker cannot be reverted.\n";

        return false;
    }
    */
}
