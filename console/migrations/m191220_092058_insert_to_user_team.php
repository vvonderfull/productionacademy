<?php

use yii\db\Migration;
use Faker\Factory;
/**
 * Class m191220_092058_insert_to_user_team
 */
class m191220_092058_insert_to_user_team extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        for ($i = 1; $i <= 30; $i++)
        {
            $posts = [];
            $posts[] = [
                $faker->unique()->numberBetween(10,40),
                $i+10,
                $faker->numberBetween(1,21),
                $faker->date(),
            ];
            $this->batchInsert('{{%user_team}}',['id','user_id','team_id','created_at'],$posts);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 1; $i < 31 ; $i++) {
            $this->delete('{{%user_team}}', ['in', 'id', [$i]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191220_092058_insert_to_user_team cannot be reverted.\n";

        return false;
    }
    */
}
