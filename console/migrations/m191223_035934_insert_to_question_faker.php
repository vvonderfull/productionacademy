<?php

use yii\db\Migration;
use Faker\Factory;

/**
 * Class m191223_035934_insert_to_question_faker
 */
class m191223_035934_insert_to_question_faker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        for($i = 0; $i < 1; $i++)
        {
            $posts = [];
            $i = 200;
            for ($j = 0; $j < 20; $j++)
            {
                $posts[] = [
                    $j+200,
                    $faker->text(20),
                    $i,
                ];
                $i+=4;
            }
            $this->batchInsert('{{%question}}',['id','text','right_answer_id'],$posts);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 0; $i < 20; $i++) {
            $this->delete('{{%question}}', ['in', 'id', [$i + 200]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191223_035934_insert_to_question_faker cannot be reverted.\n";

        return false;
    }
    */
}
