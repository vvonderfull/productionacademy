<?php

use Faker\Factory;
use yii\db\Migration;

/**
 * Class m191223_041137_insert_to_answer_faker
 */
class m191223_041137_insert_to_answer_faker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        $posts = [];
        $id = 200;
        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $posts[] =
                    [
                        $id++,
                        $faker->text(20),
                        $i + 200,
                    ];
            };
        }

        $this->batchInsert('{{%answer}}', ['id', 'text', 'question_id'], $posts);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $posts = [];
        $id = 200;
        for ($i = 0; $i < 20; $i++) {
            for ($j = 0; $j < 4; $j++) {
                $this->delete('{{%answer}}', ['in', 'id', [$id++]]);
            };
        };

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191223_041137_insert_to_answer_faker cannot be reverted.\n";

        return false;
    }
    */
}
