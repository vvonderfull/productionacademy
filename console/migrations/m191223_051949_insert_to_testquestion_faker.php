<?php

use Faker\Factory;
use yii\db\Migration;

/**
 * Class m191223_051949_insert_to_testquestion_faker
 */
class m191223_051949_insert_to_testquestion_faker extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $faker = Factory::create();
        $posts = [];
        for ($i = 200; $i < 220; $i++) {
            $posts[] =
                [
                    $faker->numberBetween(2, 5),
                    $i,
                ];
        }
        $this->batchInsert('{{%test_question}}', ['test_id', 'question_id'], $posts);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 0; $i < 20; $i++) {
            $this->delete('{{%test_question}}', ['in', 'question_id', [$i + 200]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m191223_051949_insert_to_testquestion_faker cannot be reverted.\n";

        return false;
    }
    */
}
