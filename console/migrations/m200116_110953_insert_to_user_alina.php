<?php

use yii\db\Migration;

/**
 * Class m200116_110953_insert_to_user_alina
 */
class m200116_110953_insert_to_user_alina extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[6,1579841507,'kirillastrov@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 6 ,'asdasd' , 6]]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'id', ['6']]);
        $this->delete('{{%token}}', ['in', 'id', [6]]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200116_110953_insert_to_user_alina cannot be reverted.\n";

        return false;
    }
    */
}
