<?php

use yii\db\Migration;

/**
 * Class m200126_061203_insert_to_user_egor
 */
class m200126_061203_insert_to_user_egor extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[7,1579841507,'egooorila74@gmail.com','Egor Super Manager','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 7 ,'egor_vot_tvoy_token_ne_poterjaj' , 7]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'password_hash', 'fio','phone','status','updated_at'],
            [[8,1579841507,'niceswap@mail.ru','$2y$13$fxpB6bZ.G90XsW6CrNgqsuvcl5FEmCtN73wxQi7IroPFm96hB83GK','Erdem Admin User','89969387058',15,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 8 ,'niceswap' , 8]]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'id', [7]]);
        $this->delete('{{%token}}', ['in', 'id', [7]]);

        $this->delete('{{%user}}', ['in', 'id', [8]]);
        $this->delete('{{%token}}', ['in', 'id', [8]]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_061203_insert_to_user_egor cannot be reverted.\n";

        return false;
    }
    */
}
