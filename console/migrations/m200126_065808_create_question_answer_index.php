<?php

use yii\db\Migration;

/**
 * Class m200126_065808_create_question_answer_index
 */
class m200126_065808_create_question_answer_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-question_right_answer_id',
            'question',
            'right_answer_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-question_right_answer_id',
            'question'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_065808_create_question_answer_index cannot be reverted.\n";

        return false;
    }
    */
}
