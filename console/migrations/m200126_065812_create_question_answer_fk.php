<?php

use yii\db\Migration;

/**
 * Class m200126_065812_create_question_answer_fk
 */
class m200126_065812_create_question_answer_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-question-right-answer_id',
            'question',
            'right_answer_id',
            'answer',
            'id',
            'SET NULL',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-question-right-answer_id',
            'question'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200126_065812_create_question_answer_fk cannot be reverted.\n";

        return false;
    }
    */
}
