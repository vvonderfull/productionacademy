<?php

use yii\db\Migration;

/**
 * Class m200201_054139_insert_to_test
 */
class m200201_054139_insert_to_test extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%test}}',
            ['id', 'path', 'stage', 'max_points','label'],
            //Тесты по софтскилам
            [[7, 'https://testserver.pro/run/test/Oprosnik-Tomasa/', 1, 11 ,'Уступок'],
                [8, 'https://testserver.pro/run/test/Oprosnik-Tomasa/', 1, 12,'Противоборство'],
                [9, 'https://testserver.pro/run/test/Oprosnik-Tomasa/', 1, 12, 'Сотрудничество'],
                [10, 'https://testserver.pro/run/test/Oprosnik-Tomasa/', 1, 12, 'Компромисс'],
                [11, 'https://testserver.pro/run/test/Oprosnik-Tomasa/', 1, 13,'Избегание'],

                //Тесты по харде по ролям (1 - менеджер и тд)
                [12, '', 2, 50,''],
                [13, '', 2, 50,''],

                //Аналитика
                [14, 'https://onlinetestpad.com/ru/test/5361-test-na-analiticheskoe-myshlenie', 3, null,''],
                [15, 'http://www.test-machine.ru/testi-dlya-proverki-intellektualnih-sposobnostey/test-na-logicheskoe-mishlenie/', 3, 30,''],
                [16, 'https://digitaltests.ru/verbalnyy-test-online/', 3, null,''],
                [17, 'https://docs.google.com/forms/d/1V3pfNxB4nCiV3D7LwmjQ2nuwU2w1dngEFv-WBO3lHx8/edit', 3, 14,''],

                //Front
                [18, 'https://docs.google.com/forms/d/1P0hZJERJOWEYqwKQLhxtGNh8egUfOBBKlAbav2Jo_tY/edit', 3, 15,''],
                [19, 'https://docs.google.com/forms/d/18M6fK4tfVRcEq6Ok2r3ZEKUsLSQMyt2HkblS2ZEyVsI/edit', 3, 15,''],
                [20, 'https://docs.google.com/forms/d/1XQGgBWbdK6oSn_Mr3i1YXWD0EarDX41IGPUMvsYABcc/edit', 3, 15,''],
                [21, 'https://docs.google.com/forms/d/1fFsFwJ8kjGeCYmlZmQHKpS5gG4E3PJaTcnG0GHMQ0Ik/viewform?edit_requested=true', 3, 6,''],

                //Back
                [22, 'https://webshake.ru/test-php', 3, null,''],
                [23, 'https://docs.google.com/forms/d/1N7BADmv_qwE7jcBTQvmiozTgRA04KqfbU8Ke2HsPR-k/edit', 3, 20,''],

                //Testers
                [24, 'https://docs.google.com/forms/d/1khK0y6ifVZ9IomRGdeuDT6petYtl97f2Q3vK7E05jOM/viewform?edit_requested=true', 3, 17,''],
                [25, 'https://docs.google.com/forms/d/1i75mxPZk6sOx5sCDZ2NNl6JkCke6PrP-u8a7tIh8ILs/viewform?edit_requested=true', 3, 15,''],
            ]);
    }
    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 7; $i < 26; $i++) {
            $this->delete('{{%test}}', ['in', 'id', [$i]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_054139_insert_to_test cannot be reverted.\n";

        return false;
    }
    */
}
