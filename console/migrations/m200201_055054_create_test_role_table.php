<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%test_role}}`.
 */
class m200201_055054_create_test_role_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%test_role}}', [
            'test_id' => $this->integer()->notNull(),
            'role_id' => $this->integer()->notNull(),
            'PRIMARY KEY(test_id, role_id)',
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%test_role}}');
    }
}
