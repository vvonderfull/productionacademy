<?php

use yii\db\Migration;

/**
 * Class m200201_055428_create_testrole_test_index
 */
class m200201_055428_create_testrole_test_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-test-role-test_id',
            'test_role',
            'test_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-test-role-test_id',
            'test_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_055428_create_testrole_test_index cannot be reverted.\n";

        return false;
    }
    */
}
