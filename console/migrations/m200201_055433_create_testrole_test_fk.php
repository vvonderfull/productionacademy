<?php

use yii\db\Migration;

/**
 * Class m200201_055433_create_testrole_test_fk
 */
class m200201_055433_create_testrole_test_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-test-role-test_id',
            'test_role',
            'test_id',
            'test',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-test-role-test_id',
            'test_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_055433_create_testrole_test_fk cannot be reverted.\n";

        return false;
    }
    */
}
