<?php

use yii\db\Migration;

/**
 * Class m200201_055440_create_testrole_role_index
 */
class m200201_055440_create_testrole_role_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-test-role-role_id',
            'test_role',
            'role_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-test-role-role_id',
            'test_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_055440_create_testrole_role_index cannot be reverted.\n";

        return false;
    }
    */
}
