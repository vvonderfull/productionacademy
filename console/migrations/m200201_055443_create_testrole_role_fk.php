<?php

use yii\db\Migration;

/**
 * Class m200201_055443_create_testrole_role_fk
 */
class m200201_055443_create_testrole_role_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-test-role-role_id',
            'test_role',
            'role_id',
            'role',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-test-role-role_id',
            'test_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_055443_create_testrole_role_fk cannot be reverted.\n";

        return false;
    }
    */
}
