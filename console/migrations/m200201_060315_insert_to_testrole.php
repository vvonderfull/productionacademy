<?php

use yii\db\Migration;

/**
 * Class m200201_060315_insert_to_testrole
 */
class m200201_060315_insert_to_testrole extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%test_role}}',['test_id','role_id'],
            // ТЕсты от академии
        [
//            [[1 , 1],
                [2 , 2],
                [3 , 3],
                [4 , 4],
                [5 , 5],
//                [6 , 6],

                //Тесты по софтскилам
                [7 , 1], [7 , 2], [7 , 3], [7 , 4], [7 , 5], [7 , 6],
                [8 , 1], [8 , 2], [8 , 3], [8 , 4], [8 , 5], [8 , 6],
                [9 , 1], [9 , 2], [9 , 3], [9 , 4], [9 , 5], [9 , 6],
                [10 , 1], [10 , 2], [10 , 3], [10 , 4], [10 , 5], [10 , 6],
                [11 , 1], [11 , 2], [11 , 3], [11 , 4], [11 , 5], [11 , 6],

                //Тесты по харде
                [12 , 1], [13 , 1],
                [14 , 2], [15 , 2], [16 , 2], [17 , 2],
                [18 , 3], [19 , 3], [20 , 3], [21 , 3],
                [22 , 4], [23 , 4],
                [24 , 5], [25 , 5],
            ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%test_question}}', ['in', ['test_id','question_id'],
            [[2,21],
                [2,22],
                [2,23],
                [2,24],
                [2,25],
                [2,26]]]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200201_060315_insert_to_testrole cannot be reverted.\n";

        return false;
    }
    */
}
