<?php

use yii\db\Migration;

/**
 * Class m200206_115452_create_userrole_test_index
 */
class m200206_115452_create_userrole_test_index extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex(
            'idx-user-role-test_id',
            'user_role',
            'test_id'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex(
            'idx-user-role-test_id',
            'user_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200206_115452_create_userrole_test_index cannot be reverted.\n";

        return false;
    }
    */
}
