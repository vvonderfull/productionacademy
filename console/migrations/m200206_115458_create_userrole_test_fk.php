<?php

use yii\db\Migration;

/**
 * Class m200206_115458_create_userrole_test_fk
 */
class m200206_115458_create_userrole_test_fk extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addForeignKey(
            'fk-user-role-test_id',
            'user_role',
            'test_id',
            'test',
            'id',
            'CASCADE',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropForeignKey(
            'fk-user-role-test_id',
            'user_role'
        );
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200206_115458_create_userrole_test_fk cannot be reverted.\n";

        return false;
    }
    */
}
