<?php

use yii\db\Migration;

/**
 * Class m200209_064514_insert_to_user_leads
 */
class m200209_064514_insert_to_user_leads extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at', 'is_send'],
            [
                [50,1581259622,'iulkaa@yandex.ru','Юлия','8 (961) 727-43-46',11,1581259622, true],
                [51,1581259622,'sologubovdi@mail.ru','Дмитрий','8 (913) 108-23-66',11,1581259622, true],
                [52,1581259622,'mrzankai@gmail.com','Артем','8 (950) 393-29-78',11,1581259622, true],
                [53,1581259622,'nastyaoseeva1998@gmail.com','Анастасия','8 (914) 056-93-01',11,1581259622, true],
                [54,1581259622,'ges9595@yandex.ru','Евгений','8 (999) 619-12-39',11,1581259622, true],
                [55,1581259622,'zigi1995@mail.ru','Андрей','8 (923) 403-40-93',11,1581259622, true],
                [56,1581259622,'gmitron01@gmail.com','Алексей','8 (913) 883-42-67',11,1581259622, true],
                [57,1581259622,'akane_ame@mail.ru','Яна','8 (961) 095-04-49',11,1581259622, true],
                [58,1581259622,'allabelikova@icloud.com','Алла','8 (913) 109-79-57',11,1581259622, true],
                [59,1581259622,'evgeniyurbanovskiy@gmail.com','Евгений','8 (953) 912-99-11',11,1581259622, true],
                [60,1581259622,'dranitcyna99@gmail.com','Вероника Драницына','8 (923) 403-26-76',11,1581259622, true],
                [61,1581259622,'valevich.nastya@mail.ru','Анастасия','8 (909) 538-08-45',11,1581259622, true],
                [62,1581259622,'marzaeva1993@gmail.com','Виктория Марзаева','8 (999) 618-83-43',11,1581259622, true],
                [63,1581259622,'zkochurin@mail.ru','Захар','8 (983) 239-11-90',11,1581259622, true],
                [64,1581259622,'lapardin.andrey@mail.ru','Андрей','8 (924) 657-20-18',11,1581259622, true],
                [65,1581259622,'vserovikova85@gmail.com','Вероника','8 (999) 618-97-88',11,1581259622, true],
                [66,1581259622,'marietta.defo@gmail.c om','Мария','8 (952) 683-61-38',11,1581259622, true],
                [67,1581259622,'kondrateva_005@mail.ru','Марина','8 (913) 305-00-62',11,1581259622, true],
                [68,1581259622,'open.set6@gmail.com','Искандар','8 (952) 178-81-33',11,1581259622, true],
                [69,1581259622,'veronika16547@gmail.com','Вероника','8 (906) 932-59-77',11,1581259622, true],
                [70,1581259622,'Murotov99@bk.ru','Фазлиддин','8 (952) 893-13-75',11,1581259622, true],
                [71,1581259622,'n_belaev@mail.ru','Никита','8 (913) 881-37-37',11,1581259622, true],
                [72,1581259622,'ziphir11@mail.ru','Дмитрий','8 (952) 806-28-61',11,1581259622, true],
                [73,1581259622,'iulianaorel@gmail.com','Иулиана Орел','8 (923) 425-80-69',11,1581259622, true],
                [74,1581259622,'mongush_damyrak@bk.ru','Дамырак','8 (999) 485-28-22',11,1581259622, true],
                [75,1581259622,'dvk61@tpu.ru','Дарья','8 (923) 415-71-39',11,1581259622, true],
                [76,1581259622,'pikulinadasha99@gmail.com','Дарья','8 (913) 865-13-18',11,1581259622, true],

                [77,1581259622,'Kalmarko.dima@gmail.com','Дмитрий Скоробогатько','8 (996) 937-03-60',11,1581259622, true],

                [78,1581259622,'nastyt41@mail.com','Анастасия','8 (999) 499-59-12',11,1581259622, true],
                [79,1581259622,'nikster.nikster@gmail.com','Никита','8 (953) 923-39-42',11,1581259622, true],
                [80,1581259622,'alexv0412@gmail.com','Александр','8 (996) 937-87-97',11,1581259622, true],
                [81,1581259622,'golding27111999@gmail.com','Евгений','8 (961) 095-00-31',11,1581259622, true],
                [82,1581259622,'0mata@mail.ru','Владислав','8 (952) 162-61-63',11,1581259622, true],
                [83,1581259622,'felkreelive@gmail.com','Диана Шутова','8 (923) 414-23-21',11,1581259622, true],
                [84,1581259622,'kuhtamd@mail.ru','Михаил','8 (923) 434-14-40',11,1581259622, true],
                [85,1581259622,'anastasiamal2001@gmail.com','Анастасия','8 (923) 437-08-68',11,1581259622, true],
                [86,1581259622,'ramazan.ualdanov@mail.ru','Рамазан','8 (962) 782-00-61',11,1581259622, true],
                [87,1581259622,'oxigenx6@yandex.ru','Дмитрий','8 (952) 888-46-18',11,1581259622, true],

                [88,1581259622,'Raukoaina84@gmail.com','Ирина','8 (952) 899-33-19',11,1581259622, true],

                [89,1581259622,'polisha172000@mail.ru','Полина','8 (983) 162-72-54',11,1581259622, true],
                [90,1581259622,'aom21@tpu.ru','Анастасия','8 (923) 437-08-68',11,1581259622, true],
                [91,1581259622,'tszzzch@gmail.com','Зорикма','8 (914) 059-06-07',11,1581259622, true],
                [92,1581259622,'silverusdragonitos@yandex.ru','Михаил','8 (952) 156-96-49',11,1581259622, true],

                //Неверный пользователь
                [93,1581259622,'tonako9797@gmail.com','Тилек','8 (952) 899-58-08',11,1581259622, true],

            ]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [
                ['2020-02-10 14:47:02' , 10 ,'VZBYg9lbWkNh22GKgDtjX0aN0-xpzgoi' , 50],
                ['2020-02-10 14:47:02' , 11 ,'Ci2n555RXjGGDtUS-m4daFkBTe4Dtp28' , 51],
                ['2020-02-10 14:47:02' , 12 ,'NAUFJItqa0Q2240kYSEeYT19puQLXmYE' , 52],
                ['2020-02-10 14:47:02' , 13 ,'tQbvNbq55Uv7onT9Cqg-mJAdqrDwUyfP' , 53],
                ['2020-02-10 14:47:02' , 14 ,'tPQDYVVNFq3O1Pe1AUKODgTh7JyuWdsk' , 54],
                ['2020-02-10 14:47:02' , 15 ,'7Jp6WigBB7xw7kV5yQTB71k7KpZkIAxZ' , 55],
                ['2020-02-10 14:47:02' , 16 ,'LCKi2bfNQ5CBLylnMowyVRBSiHlDGPzZ' , 56],
                ['2020-02-10 14:47:02' , 17 ,'UeT4AFhobvIKklclrlI0kPMBTBbJU4p_' , 57],
                ['2020-02-10 14:47:02' , 18 ,'ZyJU5q3rFgO3Jrs4JPANNyb513N9Nwrg' , 58],
                ['2020-02-10 14:47:02' , 19 ,'Owx6bNhKKQTxuSZlFGSNDTlyEft6GZEd' , 59],
                ['2020-02-10 14:47:02' , 20 ,'ZJ_a9eb3m1YIsrFPV1WEE3jP6MdyTZmb' , 60],
                ['2020-02-10 14:47:02' , 21 ,'WdhcH3Qtj38NSBTmHXql-j-JBu8QFoXm' , 61],
                ['2020-02-10 14:47:02' , 22 ,'Yb4TIp5W42F_OP0NS8pF_1nBEgWqVODu' , 62],
                ['2020-02-10 14:47:02' , 23 ,'2BjBwJ0q84Xkpe9SMiokLMZKUaTdhSxJ' , 63],
                ['2020-02-10 14:47:02' , 24 ,'9sUL0G3CpaKZcIqQccjN0g_I1KRqKRBa' , 64],
                ['2020-02-10 14:47:02' , 25 ,'dcDHUhbJJ6c4kh18E1aYcJIfYwnvkDlO' , 65],
                ['2020-02-10 14:47:02' , 26 ,'-EzvTNJZ-kyVdNT1FuuahfRgfH6IDzFR' , 66],
                ['2020-02-10 14:47:02' , 27 ,'7to82iL5qFzYWwHA9dXPV5DnddRSsX1P' , 67],
                ['2020-02-10 14:47:02' , 28 ,'lcoamd6tnE9fhi020Lo6ruVhWA4HLLr4' , 68],
                ['2020-02-10 14:47:02' , 29 ,'U63UhTyhk11iWAlcYoJJEJX7EnG6h3Wr' , 69],
                ['2020-02-10 14:47:02' , 30 ,'a6ZWsiKbthbC_KtHZ2js_BVi1soTnVQB' , 70],
                ['2020-02-10 14:47:02' , 31 ,'i04JJhKbCGPoGE7Dle0v3mspXprJloUd' , 71],
                ['2020-02-10 14:47:02' , 32 ,'0kvb-RUUlCrRBkI85VJ0UW8pEdT95E_b' , 72],
                ['2020-02-10 14:47:02' , 33 ,'o122e8eV7AtHQtZuFEvQoHaEpCYMQwwn' , 73],
                ['2020-02-10 14:47:02' , 34 ,'b4PIh7Z7CVuvpQDT7A0yxFmnijJDIJOy' , 74],
                ['2020-02-10 14:47:02' , 35 ,'vrN_gU9_K9o7hXRd-dVvBVPCV3SJccag' , 75],
                ['2020-02-10 14:47:02' , 36 ,'TASSFkPt38uiDHw_tESApLN2e9_1wGQi' , 76],
                ['2020-02-10 14:47:02' , 37 ,'-uvDaPVnsmaNhYVtneKzWafY3C1aIJkC' , 77],
                ['2020-02-10 14:47:02' , 38 ,'sY8T3RHO_ZSkw37rZ92prrdj-8TiSngv' , 78],
                ['2020-02-10 14:47:02' , 39 ,'cxGyrHQQWlg_psKDtRQQXvup9-jCDRCM' , 79],
                ['2020-02-10 14:47:02' , 40 ,'t9rGdv8nrHYQujsLbefkjE3DDoD0unuI' , 80],
                ['2020-02-10 14:47:02' , 41 ,'272ziCY7Wh36afhrmSb2DIJhU3RJ376M' , 81],
                ['2020-02-10 14:47:02' , 42 ,'MeINSgTL1Yy-qDXteAK7HnHsALebB3e6' , 82],
                ['2020-02-10 14:47:02' , 43 ,'J_dFxEamMSR-xQLxqmlOw4BwV8Ohc644' , 83],
                ['2020-02-10 14:47:02' , 44 ,'gLvG1Qjw5sSEJlaYW_ISoPT6M8osAYCb' , 84],
                ['2020-02-10 14:47:02' , 45 ,'U2ooLWU2Lgske2WlfjBfNS2lGy-nUA95' , 85],
                ['2020-02-10 14:47:02' , 46 ,'-NOmFs8LoioYg0rHtI7Q35iOQGenNnka' , 86],
                ['2020-02-10 14:47:02' , 47 ,'WVXm3Rj1I9uLm_ksMbBWuK1bdpn6zSm-' , 87],
                ['2020-02-10 14:47:02' , 48 ,'SRvW1HnRZ4XgxTqnTxHxJK8QYqmjkTmx' , 88],
                ['2020-02-10 14:47:02' , 49 ,'uQXte0gveav8nk-07XueQfbGwrMRftH5' , 89],
                ['2020-02-10 14:47:02' , 50 ,'bFgAKX7jze25m5VbZdwzwMSwY2MAQ5e-' , 90],
                ['2020-02-10 14:47:02' , 51 ,'g2xxzahZOkuy46MWJyN7Y8jRmIZcppI4' , 91],
                ['2020-02-10 14:47:02' , 52 ,'WmHU8Hga9i8qt5rE21-bIqby-2h70Bvr' , 92],
                ['2020-02-10 14:47:02' , 53 ,'2cZya9Dc-Fkbf-Y7UYiTCEafN95Sw4ZB' , 93],
                ]);

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        for ($i = 10; $i <= 53; $i++) {
            $this->delete('{{%token}}', ['in', 'id', [$i]]);
        }
        for ($i = 50; $i <= 93; $i++) {
            $this->delete('{{%user}}', ['in', 'id', [$i]]);
        }
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200209_064514_insert_to_user_leads cannot be reverted.\n";

        return false;
    }
    */
}
