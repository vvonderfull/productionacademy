<?php

use yii\db\Migration;

/**
 * Class m200214_043100_delete_test_analitic
 */
class m200214_043100_delete_test_analitic extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
            $this->delete('{{%test}}', ['in', 'id', [16]]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200214_043100_delete_test_analitic cannot be reverted.\n";

        return false;
    }
    */
}
