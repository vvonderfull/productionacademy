<?php

use yii\db\Migration;

/**
 * Class m200214_044600_update_tests_table
 */
class m200214_044600_update_tests_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%test}}','description','string');
        $this->addColumn('{{%test}}','header','string');

        $this->update('{{%test}}',['label'=>'Уступка'],['label'=>'Уступок']);

        for ($i = 7; $i <= 11; $i++) {
            $this->update('{{%test}}', ['description' => 'результаты вводить в баллах'], ['id'=>$i]);
        }
        for ($i = 7; $i <= 11; $i++) {
            $this->update('{{%test}}', ['header' => 'Опросник Томаса'], ['id'=>$i]);
        }

        //Аналитик
        $this->update('{{%test}}', ['description' => 'результат вводить в процентах'], ['id'=>14]);
        $this->update('{{%test}}', ['header' => 'Тест на аналитическое мышление'], ['id'=>14]);
        $this->update('{{%test}}', ['description' => 'результат вводить в баллах'], ['id'=>15]);
        $this->update('{{%test}}', ['header' => 'Тест на логическое мышление'], ['id'=>15]);
        $this->update('{{%test}}', ['description' => 'результат вводить в баллах'], ['id'=>17]);
        $this->update('{{%test}}', ['header' => 'Тест на анализ информации'], ['id'=>17]);

        //Фронт
        $this->update('{{%test}}', ['header' => 'Тест по HTML'], ['id'=>18]);
        $this->update('{{%test}}', ['header' => 'Тест по CSS'], ['id'=>19]);
        $this->update('{{%test}}', ['header' => 'Тест по JAVASCRIPT'], ['id'=>20]);
        $this->update('{{%test}}', ['header' => 'Тест по VueJS'], ['id'=>21]);

        for ($i = 18; $i <= 21; $i++) {
            $this->update('{{%test}}', ['description' => 'результаты вводить в баллах'], ['id'=>$i]);
        }

        $this->update('{{%test}}', ['header' => 'Тест по PHP'], ['id'=>22]);
        $this->update('{{%test}}', ['header' => 'Тест по основам бэкенда'], ['id'=>23]);
        $this->update('{{%test}}', ['description' => 'результат вводить в процентах'], ['id'=>22]);
        $this->update('{{%test}}', ['description' => 'результат вводить в баллах'], ['id'=>23]);

        $this->update('{{%test}}', ['header' => 'Тест на внимание и сосредоточенность'], ['id'=>24]);
        $this->update('{{%test}}', ['header' => 'Тест по логическое мышление'], ['id'=>25]);
        $this->update('{{%test}}', ['description' => 'результат вводить в баллах'], ['id'=>24]);
        $this->update('{{%test}}', ['description' => 'результат вводить в баллах'], ['id'=>25]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropColumn('{{%test}}','description');
        $this->dropColumn('{{%test}}','header');
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200214_044600_update_tests_table cannot be reverted.\n";

        return false;
    }
    */
}
