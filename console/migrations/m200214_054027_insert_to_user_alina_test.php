<?php

use yii\db\Migration;

/**
 * Class m200214_054027_insert_to_user_alina_test
 */
class m200214_054027_insert_to_user_alina_test extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[94,1579841507,'test1@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 54 ,'test1' , 94]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[95,1579841507,'test2@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 55 ,'test2' , 95]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[96,1579841507,'test3@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 56 ,'test3' , 96]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[97,1579841507,'test4@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 57 ,'test4' , 97]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[98,1579841507,'test5@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 58 ,'test5' , 98]]);

        $this->batchInsert('{{%user}}',['id','created_at','email', 'fio','phone','status','updated_at'],
            [[99,1579841507,'test6@gmail.com','Alina Super Tester','8 (800) 555-35-35',11,1579841507]]);
        $this->batchInsert('{{%token}}',['expired_at','id','token','user_id'],
            [['2020-01-01' , 59 ,'test6' , 99]]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->delete('{{%user}}', ['in', 'id', [95]]);
        $this->delete('{{%token}}', ['in', 'id', [55]]);

        $this->delete('{{%user}}', ['in', 'id', [96]]);
        $this->delete('{{%token}}', ['in', 'id', [56]]);

        $this->delete('{{%user}}', ['in', 'id', [97]]);
        $this->delete('{{%token}}', ['in', 'id', [57]]);

        $this->delete('{{%user}}', ['in', 'id', [98]]);
        $this->delete('{{%token}}', ['in', 'id', [58]]);

        $this->delete('{{%user}}', ['in', 'id', [99]]);
        $this->delete('{{%token}}', ['in', 'id', [59]]);

        $this->delete('{{%user}}', ['in', 'id', [94]]);
        $this->delete('{{%token}}', ['in', 'id', [54]]);
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200214_054027_insert_to_user_alina_test cannot be reverted.\n";

        return false;
    }
    */
}
