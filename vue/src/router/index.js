import Vue from 'vue'
import VueRouter from 'vue-router'
import Supervisor from "../views/Supervisor"
import formedTeam from '../views/formedTeam'
import unformedTeam from '../views/unformedTeam'
import StudentRegistration from '../views/StudentRegistration'
import TestSoft from "../views/TestSoft";
import TestHard from "../views/TestHard";
import Auth from "../views/Auth";
import TestPresentSoft from "../views/TestPresentSoft";
import TestPresentHard from "../views/TestPresentHard";
import TestComplete from "../views/TestComplete";
import StudentCabinet from '../views/StudentCabinet'
import Mentor from "../views/Mentor";
import store from '../store'

Vue.use(VueRouter)

const routes = [
    {
        path: '/supervisor',
        name: 'supervisor',
        component: Supervisor,
        meta: {
            requiresAuth: true,
            layout: 'supervisor'
        },
        children: [
            {
                path: '/supervisor/formedTeam',
                name: 'formedTeam',
                component: formedTeam,
                meta: {
                    requiresAuth: true,
                    layout: 'supervisor'
                }
            },
            {
                path: '/supervisor/unformedTeam',
                name: 'unformedTeam',
                component: unformedTeam,
                meta: {
                    requiresAuth: true,
                    layout: 'supervisor'
                }
            }
        ],
    },
    {
        path: '/studentRegistration',
        name: 'studentRegistration',
        component: StudentRegistration,
        meta: {
            requiresAuth: false,
            layout: 'registration'
        }
    },
    {
        path: '/testSoft',
        name: 'testSoft',
        component: TestSoft,
        meta: {
            requiresAuth: false,
            layout: 'auth'
        }
    },
    {
        path: '/testHard',
        name: 'testHard',
        component: TestHard,
        meta: {
            requiresAuth: false,
            layout: 'auth'
        }
    },
    {
        path: '/studentCabinet',
        name: 'studentCabinet',
        component: StudentCabinet,
        meta: {
            requiresAuth: false,
            layout: 'main'
        }
    },
    {
        path: '/',
        name: 'auth',
        component: Auth,
        meta: {
            layout: 'auth'
        }
    },
    {
        path: '/testPresentSoft',
        name: 'testPresentSoft',
        component: TestPresentSoft,
        meta: {
            requiresAuth: false,
            layout: 'auth'
        }
    },
    {
        path: '/testPresentHard',
        name: 'testPresentHard',
        component: TestPresentHard,
        meta: {
            requiresAuth: false,
            layout: 'auth'
        }
    },
    {
        path: '/testComplete',
        name: 'testComplete',
        component: TestComplete,
        meta: {
            requiresAuth: false,
            layout: 'main'
        }
    },
    {
        path: '/mentor',
        name: 'mentor',
        component: Mentor,
        meta: {
            requiresAuth: true,
            layout: 'main'
        }
    },
]

const router = new VueRouter({
    mode: 'history',
    base: process.env.BASE_URL,
    routes
})

router.beforeEach((to, from, next) => {
    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (store.getters.isLoggedIn) {
            next()
            return
        }
        next('/')
    } else {
        next()
    }
})

export default router
