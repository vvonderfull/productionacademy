import Axios from 'axios'

export default {
    state: {
        status: '',
        token: localStorage.getItem('token') || '',
        user: {},
    },
    mutations: {
        auth_request(state) {
            state.status = 'loading'
        },
        auth_success(state, token, user) {
            state.status = 'success'
            state.token = token
            state.user = user
        },
        auth_error(state) {
            state.status = 'error'
        },
        logout(state) {
            state.status = ''
            state.token = ''
        },
    },
    actions: {
        async login({dispatch, commit}, formData) {
            return new Promise((resolve, reject) => {
                commit('auth_request')
                Axios({
                    url: 'http://api.mentor.student.smartworld.team:2280/login',
                    data: {
                        email: formData.email,
                        password: formData.password,
                    },
                    method: 'POST',
                    headers: {
                        'Content-Type': 'application/json'
                    }
                })
                    .then(resp => {
                        const token = resp.data.token
                        const status = resp.data.status
                        localStorage.setItem('token', token)
                        localStorage.setItem('status', status)
                        commit('auth_success', token)
                        resolve(resp)
                    })
                    .catch(err => {
                        commit('auth_error')
                        localStorage.removeItem('token')
                        reject(err)
                        console.log(err)
                    })
            })
        },
        logout({commit}) {
            return new Promise((resolve, reject) => {
                commit('logout')
                localStorage.removeItem('token')
                localStorage.removeItem('status')
                delete Axios.defaults.headers.common['Authorization']
                resolve()
            })
        },
        messageWarn({commit}) {
            this.$toastr.warning('Нет прав!', {
                closeButton: true,
                timeOut: 3000,
            })
        }
    },
    getters: {
        isLoggedIn: state => !!state.token,
        authStatus: state => state.status,
    }
}
