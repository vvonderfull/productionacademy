import Axios from "axios";

export default {
    state: {
        href: ''
    },
    mutations: {},
    actions: {

        getExelTeams: async function ({commit}, url) {
            Axios({
                method: 'GET',
                url: url + '/get-excel-teams',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
                .then((resp) => {
                    let href = 'http://api.mentor.student.smartworld.team:2280/get-excel-teams?token=' + localStorage.getItem('token')
                    window.open(href)
                })
                .catch(function (error) {
                    console.log(error)
                });
        },
        async getExelRoles({commit}, url) {
            Axios({
                method: 'GET',
                url: url + '/get-excel-roles',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
                .then(function (response) {
                    let href = 'http://api.mentor.student.smartworld.team:2280/get-excel-roles?token=' + localStorage.getItem('token')
                    window.open(href)
                })
                .catch(function (error) {
                    console.log(error)
                });
        }
    }
}
