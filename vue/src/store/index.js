import Vue from 'vue'
import Vuex from 'vuex'
import registrationStudent from './registrationStudent'
import auth from './auth'
import testSoft from './testSoft'
import testHard from './testHard'
import students from "./students"
import newRecruting from './newRecruting'
import autoFormation from "./autoFormation";
import exel from "./exel";

Vue.use(Vuex)

export default new Vuex.Store({
    state: {
        url: 'http://api.mentor.student.smartworld.team:2280',
    },
    mutations: {},
    actions: {},
    modules: {
        registrationStudent,
        auth,
        testSoft,
        testHard,
        newRecruting,
        students,
        autoFormation,
        exel
    }
})
