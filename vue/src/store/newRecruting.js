import Axios from 'axios'

export default {
    state: {
        leads: {}
    },
    mutations: {
        setLeads(state, response) {
            state.leads = response
            // console.log(state.leads)
        }
    },
    actions: {
        async getLeads({commit}) {
            // console.log(localStorage.getItem('token'))
            let token = localStorage.getItem('token')
            Axios({
                method: 'get',
                url: 'http://api.mentor.student.smartworld.team:2280/leads',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            })
                .then(function (response) {
                    // console.log(response)
                    commit('setLeads', response)
                });
        },
        async startNewRecruting() {
            // console.log(localStorage.getItem('token'))
            let token = localStorage.getItem('token')
            Axios({
                method: 'post',
                url: 'http://api.mentor.student.smartworld.team:2280/send-mails',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + token
                },
            })
                .then(function (response) {
                    // console.log(response)
                });
        }
    }
}
