import Axios from "axios";

export default {
  state: {
    data: {},
    message: {},
    student:{},
  },
  actions: {
    fetchData: async function ({dispatch, commit }) {
      let token = window.location.href.split('=')
      localStorage.setItem('tokenRegistration', token[1])
      let dataStudent = {}

      // console.log(localStorage.getItem('tokenRegistration'))
      Axios({
        method: 'POST',
        url: 'http://api.mentor.student.smartworld.team:2280/send-token',
        data: {
          token: localStorage.getItem('tokenRegistration'),
        },
        headers:{
          'content-type': 'application/json',
        }
      })
        .catch(function (error) {
          console.log('error', error);
        })
        .then(function (response) {
          dataStudent = response.data
          // console.log('qq ',dataStudent)
          commit('setData', dataStudent)
        })
      },
    signUpStudent: async function ({dispatch, commit }, formData) {
      commit('setStudent', formData)
      return new Promise((resolve, reject) => {
        Axios({
          method: 'POST',
          url: 'http://api.mentor.student.smartworld.team:2280/sign-up',
          data: {
            role_name: formData.role_name,
            age: formData.age,
            password: formData.password,
            experience: formData.experience,
            study_place: formData.study_place,
            token: formData.token
          },
          headers:{
            'content-type': 'application/json'
          }
        })
          .then(resp => {
            resolve(resp)
            commit('setMessage', resp)
          })
          .catch(err => {
            reject(err)
            // console.log(err)
          })
      })
    }
  },
    mutations: {
      setData(state, dataStudent) {
        state.data = dataStudent
        // console.log(state)
      },
      setMessage(state, resp) {
        state.message = resp.data.message
        // console.log(state.message)
      },
      setStudent(state, formData) {
        state.student = formData
        // console.log(state.student)
      }
  }

}

