import Axios from 'axios'

export default {
    state: {
        teamArray: [],
        peopleArray: [{}]
    },
    mutations: {
        updateTeam(state, team) {
            state.teamArray = team
        },
        updatePeople(state, people) {
            state.peopleArray = people
        },
        changeInput(state, payload) {
            state.teamArray[payload.i].changeInput = payload.val;
        },
        spliceTeam(state, index) {
            state.teamArray.splice(index, 1)
        },
        changeName(state, payload) {
            state.teamArray[payload.index].team_name = payload.name
        }
    },
    actions: {
        getTeams: function ({commit}, url) {
            Axios({
                url: url + '/get-all-students',
                method: 'GET',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
                .then(function (res) {
                    // console.log(res);
                    const team = [];
                    const people = [];
                    for (let i = 0; i < res.data.length; i++) {
                        if (res.data[i].team_id !== 21) {
                            team.push(res.data[i]);
                            team[i].changeInput = false;
                        } else {
                            people.push(res.data[i]);
                        }
                    }
                    commit('updateTeam', team);
                    commit('updatePeople', people);
                })
                .catch(function (error) {
                    console.log('Error: ', error);
                })
        },
        autoFormation: function ({commit}, url) {
            Axios({
                url: url + '/form-teams',
                method: 'POST',
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
                .then((resp) => {
                    // console.log(resp)
                    const team = [];
                    const people = [];
                    for (let i = 0; i < resp.data.length; i++) {
                        if (resp.data[i].team_id !== 21) {
                            team.push(resp.data[i]);
                            team[i].changeInput = false;
                        } else {
                            people.push(resp.data[i]);
                        }
                    }
                    commit('updateTeam', team);
                    commit('updatePeople', people);
                })
                .catch(function (error) {
                    console.log('Error: ', error);
                })
        },
        filterStudentBoard: function ({commit, dispatch}, data) {
            Axios({
                url: data.url + '/get-hermits?sort=' + data.filterTest + '&filter=' + data.filterRole,
                method: 'POST',
                data: {
                    sort: data.filterTest,
                    filter: data.filterRole
                },
                headers: {
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + localStorage.getItem('token')
                },
            })
                .then((resp) => {
                    // console.log(resp)
                    let people = [
                        {
                            team_id: 21,
                            students: []
                        }
                    ];
                    people[0].students = resp.data
                    if (data.filterTest == 1) {
                        for (let i = 0; i <= people[0].students.length - 1; i++) {
                            people[0].students[i].total_point = people[0].students[i].stage1
                        }
                        commit('updatePeople', people);
                    } else if (data.filterTest == 3) {
                        for (let i = 0; i <= people[0].students.length - 1; i++) {
                            people[0].students[i].total_point = people[0].students[i].stage3
                        }
                        commit('updatePeople', people);
                    } else {
                        commit('updatePeople', people);
                    }
                })
                .catch(function (error) {
                    console.log('Error: ', error);
                })
        }
    },
    getters: {
        allTeam(state) {
            return state.teamArray
        },
        allPeople(state) {
            return state.peopleArray
        },
    }
}
