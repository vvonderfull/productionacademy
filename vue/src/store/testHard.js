import Axios from "axios";

export default {
    state: {
        start2: {
            name: 'Test по Hard-Skills',
            description: 'В это тесте будут выявляться ваши навыки в командной работе и взаимодействию с ней.'
        },
        questionsHard: [
            {
                name: 'Что такое ахаахахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаахах111111ахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахааха22222хахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаах33333ахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаа44444хахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаа5555хахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
        ],
        stepQuestionHard: 0,
        currentQuestionHard: {},
        currentRoleTest: '',
    },
    mutations: {
        changeQuestionHard(state) {
            state.currentQuestionHard = state.questionsHard[state.stepQuestionHard]
        },
        nextQuestionHard(state) {
            if (state.stepQuestionHard + 1 < state.questionsHard.length) {
                state.stepQuestionHard++
            }
        },
        backQuestionHard(state) {
            if (state.stepQuestionHard >= 1) {
                state.stepQuestionHard--
            }
        },
        changeCurrentRole(state, role) {
            state.currentRoleTest = role
            console.log(state.currentRoleTest)
        }
    },
    actions: {},
    getters: {
        getNameHard(state) {
            return state.start2.name
        },
        getDescriptionHard(state) {
            return state.start2.description
        },
        getQuestionsHard(state) {
            return state.questionsHard
        },
        getStepHard(state) {
            return state.stepQuestionHard
        },
        getCurrentQuestionHard(state) {
            return state.currentQuestionHard
        }
    }
}
