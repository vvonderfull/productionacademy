import Axios from "axios";

export default {
    state: {
        start1: {
            name: 'Test по Soft-Skills',
            description: 'В это тесте будут выявляться ваши навыки в командной работе и взаимодействию с ней.'
        },
        questions: [
            {
                name: 'Что такое ахаахахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаахах111111ахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахааха22222хахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаах33333ахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаа44444хахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
            {
                name: 'Что такое ахаа5555хахахаха',
                answers: [
                    {
                        answer: 'lalaalalaa',
                        value: 1
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 2
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 3
                    },
                    {
                        answer: 'lalaalalaa',
                        value: 4
                    }
                ]
            },
        ],
        stepQuestion: 0,
        currentQuestion: {}
    },
    mutations: {
        changeQuestion(state) {
            state.currentQuestion = state.questions[state.stepQuestion]
        },
        nextQuestion(state) {
            if (state.stepQuestion + 1 < state.questions.length) {
                state.stepQuestion++
            }
        },
        backQuestion(state) {
            if (state.stepQuestion >= 1) {
                state.stepQuestion--
            }
        },
    },
    actions: {

    },
    getters: {
        getName(state) {
            return state.start1.name
        },
        getDescription(state) {
            return state.start1.description
        },
        getQuestions(state) {
            return state.questions
        },
        getStep(state) {
            return state.stepQuestion
        },
        getCurrentQuestion(state) {
            return state.currentQuestion
        }
    }
}
